#include <stdio.h>
#include <stdlib.h>

#define NB_EXPERIMENTS 5000000
#define NB_FACES 10

/* -------------------------------------------------------------------------------- */
/*                                                                                  */
/* roll		Simule un lancer de dé				                    */
/*                                                                                  */
/* En sortie : Un nombre compris entre 0 et NB_FACES - 1.                           */
/*                                                                                  */
/* -------------------------------------------------------------------------------- */

int roll() {
    return rand() % NB_FACES;
}

int main() {
    // Initialisation du nombre d'apparition de chaque face

    int frequencies[NB_FACES];

    for (int j = 0; j < NB_FACES; ++j) {
        frequencies[j] = 0;
    }

    // On effectue l'expérience

    printf("Nombre d'expériences : %d\n", NB_EXPERIMENTS);
    printf("Nombre de faces : %d\n", NB_FACES);

    for (int i = 0; i < NB_EXPERIMENTS; ++i) {
        ++(frequencies[roll()]);
    }

    // Analyse des résultats

    double frequency;

    for (int j = 0; j < NB_FACES; ++j) {
        frequency = (double) frequencies[j] / NB_EXPERIMENTS;
        printf("%d - %lf\n", j, frequency);
    }

    return 0;
}
