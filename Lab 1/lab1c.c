#include <stdio.h>

/* -------------------------------------------------------------------------------- */
/*                                                                                  */
/* intRand    Implémente un générateur aléatoire congruentiel                       */
/*                                                                                  */
/* En entrée : Des entiers x, a, c et m.                                            */
/*                                                                                  */
/* En sortie : Un nouveau nombre entre 0 et m.                                      */
/*                                                                                  */
/* -------------------------------------------------------------------------------- */

int intRand(int x, int a, int c, int m) {
    return (a * x + c) % m;
}

/* -------------------------------------------------------------------------------- */
/*                                                                                  */
/* floatRand    Implémente un générateur aléatoire congruentiel normalisé           */
/*                                                                                  */
/* En entrée : Un flottant x, et des entiers a, c et m.                             */
/*                                                                                  */
/* En sortie : Un nouveau nombre entre 0 et 1.                                      */
/*                                                                                  */
/* -------------------------------------------------------------------------------- */

float floatRand(float x, int a, int c, int m) {
    return (float) intRand(m * x, a, c, m) / m;
}

int main() {
    int nb_iterations = 32;

    int a = 5, c = 1, m = 16; // Tuple utilisé dans la fonction du générateur

    int x0 = 5; // Graine initiale

    printf("Graine initiale : %d\n", x0);

    // intRand

    int x = x0;

    for (int i = 0; i < nb_iterations; ++i) {
        x = intRand(x, a, c, m);
        printf("%d ", x);
    }

    printf("\n");

    // floatRand

    float y = (float) x0 / m;

    for (int i = 0; i < nb_iterations; ++i) {
        y = floatRand(y, a, c, m);
        printf("%f ", y);
    }

    printf("\n");
    
    return 0;
}
