# Part A : Nonlinear generator

On teste plusieurs graines :
- 1234: Convergence vers 0
- 4100: Cycle (4100 -> 8100 -> 6100 -> 2100 -> 4100)
    - 4100
    - 8100
    - 6100
    - 2100
    - 4100
- 1324: Pseudo-cycle avec attente
    - 7529 
    - 6858
    - 321
    - 1030
    - 609
    - 3708
    - 7492
    - 1300
    - 6900
    - 6100
    - 2100
    - 4100
- 1301: Pseudo-cycle avec queue (Après quelques itérations, on retombre sur 4100)
- 3141: Convegence vers 100

Le générateur de nombres aléatoires inclus dans Unix n'a pas assez d'états pour être suffisamment précis. Il est donc déconseillé pour un usage scientifique.

# Part B : Basic use of randomness

> Voir le programme lab1b.c

# Part C : Crafting linear congruential generators

Choix de tuples intéressants :
- a = 33, c = 5, m = 256
- a = 35, c = 3, m = 512
- a = 31792125, c = 57, m = 2^28

> Source : https://www.ams.org/journals/mcom/1999-68-225/S0025-5718-99-00996-5/S0025-5718-99-00996-5.pdf

# Part D : Miscellaneous

Bibliothèques scientifiques implémentant des générateurs de nombres aléatoires :
- GNU Scientific Library (GSL)
    - Mersenne Twister

Bibliothèques statistiques permettant de tester la qualité d'un générateur de nombres aléatoires :
- Diehard tests
- TestU01
- NIST Statistical Test Suite