/* 
   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.

   Before using, initialize the state by using init_genrand(seed)  
   or init_by_array(init_key, key_length).

   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.                          

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote 
        products derived from this software without specific prior written 
        permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


   Any feedback is very welcome.
   http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
*/

#include <stdio.h>
#include <stdlib.h>

/* Period parameters */  
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */

static unsigned long mt[N]; /* the array for the state vector  */
static int mti=N+1; /* mti==N+1 means mt[N] is not initialized */

/* initializes mt[N] with a seed */
void init_genrand(unsigned long s)
{
    mt[0]= s & 0xffffffffUL;
    for (mti=1; mti<N; mti++) {
        mt[mti] = 
	    (1812433253UL * (mt[mti-1] ^ (mt[mti-1] >> 30)) + mti); 
        /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
        /* In the previous versions, MSBs of the seed affect   */
        /* only MSBs of the array mt[].                        */
        /* 2002/01/09 modified by Makoto Matsumoto             */
        mt[mti] &= 0xffffffffUL;
        /* for >32 bit machines */
    }
}

/* initialize by an array with array-length */
/* init_key is the array for initializing keys */
/* key_length is its length */
/* slight change for C++, 2004/2/26 */
void init_by_array(unsigned long init_key[], int key_length)
{
    int i, j, k;
    init_genrand(19650218UL);
    i=1; j=0;
    k = (N>key_length ? N : key_length);
    for (; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1664525UL))
          + init_key[j] + j; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++; j++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
        if (j>=key_length) j=0;
    }
    for (k=N-1; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1566083941UL))
          - i; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
    }

    mt[0] = 0x80000000UL; /* MSB is 1; assuring non-zero initial array */ 
}

/* generates a random number on [0,0xffffffff]-interval */
unsigned long genrand_int32(void)
{
    unsigned long y;
    static unsigned long mag01[2]={0x0UL, MATRIX_A};
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (mti >= N) { /* generate N words at one time */
        int kk;

        if (mti == N+1)   /* if init_genrand() has not been called, */
            init_genrand(5489UL); /* a default initial seed is used */

        for (kk=0;kk<N-M;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        for (;kk<N-1;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK);
        mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1UL];

        mti = 0;
    }
  
    y = mt[mti++];

    /* Tempering */
    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680UL;
    y ^= (y << 15) & 0xefc60000UL;
    y ^= (y >> 18);

    return y;
}

/* generates a random number on [0,0x7fffffff]-interval */
long genrand_int31(void)
{
    return (long)(genrand_int32()>>1);
}

/* generates a random number on [0,1]-real-interval */
double genrand_real1(void)
{
    return genrand_int32()*(1.0/4294967295.0); 
    /* divided by 2^32-1 */ 
}

/* generates a random number on [0,1)-real-interval */
double genrand_real2(void)
{
    return genrand_int32()*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on (0,1)-real-interval */
double genrand_real3(void)
{
    return (((double)genrand_int32()) + 0.5)*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on [0,1) with 53-bit resolution*/
double genrand_res53(void) 
{ 
    unsigned long a=genrand_int32()>>5, b=genrand_int32()>>6; 
    return(a*67108864.0+b)*(1.0/9007199254740992.0); 
} 
/* These real versions are due to Isaku Wada, 2002/01/09 added */


////// Fonctions écrites pour le TP //////

/* -------------------------------------------------------------------------------------------- */
/* get_distribution_function      Construit un tableau de probabilités à partir d'observations  */
/*                                                                                              */
/* En entrée : Un entier nb_classes et le tableau des observations obs                          */
/*                                                                                              */
/* En sortie : Le tableau des probabilités                                                      */
/* -------------------------------------------------------------------------------------------- */

double * get_distribution_function(int nb_classes, int obs[])
{
    // Il nous faut d'abord le nombre d'observations

    double nb_obs = 0; // Cela nous évitera de devoir convertir l'entier en réel par la suite.

    for (int i = 0; i < nb_classes; ++i) {
        nb_obs += obs[i];
    }

    // On génère ensuite le tableau des probabilités
    // (Fonction de distribution)

    double * df_array = malloc(nb_classes * sizeof(double));

    if (df_array) {
        for (int i = 0; i < nb_classes; ++i) {
            df_array[i] = obs[i] / nb_obs;
        }
    }
    
    return df_array;
}

/* ---------------------------------------------------------------------------------------- */
/* get_cumulative_distribution_function     Construit un tableau de probabilités cumulées   */
/*                                                                                          */
/* En entrée : Un entier nb_classes et un tableau de probabilités df_array                  */
/*                                                                                          */
/* En sortie : Le tableau des probabilités cumulées                                         */
/* ---------------------------------------------------------------------------------------- */

double * get_cumulative_density_function(int nb_classes, double * df_array)
{
    double * cdf_array = malloc(nb_classes * sizeof(double));

    if (cdf_array) {
        // Le premier élément du tableau correspond à la première probabilité
        cdf_array[0] = df_array[0];

        // Puis, le i-ième élément est égal à la somme des i premières probabilités

        for (int i = 1; i < nb_classes; ++i) {
            cdf_array[i] = cdf_array[i-1] + df_array[i];
        }
    }

    return cdf_array;
}

/* ---------------------------------------------------------------------------------------- */
/* get_percentages      Génère un tableau de probabilités en pourcentages                   */
/*                                                                                          */
/* En entrée : Un entier nb_classes et un tableau de probabilités array                     */
/*                                                                                          */
/* En sortie : Un nouveau tableau où les probabilités de array sont données en pourcentages */
/* ---------------------------------------------------------------------------------------- */

double * get_percentages(int nb_classes, double * array)
{
    double * pct = malloc(nb_classes * sizeof(double));

    if (pct) {
        for (int i = 0; i < nb_classes; ++i) {
            pct[i] = array[i] * 100;
        }
    }

    return pct;
}

/* -------------------------------------------------------------------------------------------- */
/* simulate_ABC     Simule une expérience selon la loi de distribution donnée dans l'énoncé :   */
/*                  A (0) : 0.35, B (1) : 0.45, C (2) : 0.2                                     */
/*                                                                                              */
/* En sortie : Une classe 0, 1, 2 générée selon la loi de distribution                          */
/* -------------------------------------------------------------------------------------------- */

int simulate_ABC()
{
    // Nous représentons les différentes classes par des entiers :
    // A : 0, B : 1 et C : 2

    // Les données sont reprises de l'énoncé et présentes "en dur" dans cette fonction.

    double psr = genrand_real1();
    int class = 2;

    if (psr < 0.35) {        // 0 <= r <= 0.35
        class = 0;
    } else if (psr < 0.8) {  // 0.35 < r <= 0.8
        class = 1;
    }

    return class;
}

/* ---------------------------------------------------------------------------------------------------- */
/* get_simulation_ABC   Simule plusieurs expériences selon la loi de distribution donnée dans l'énoncé  */
/*                      et renvoie la répartition obtenue                                               */
/*                                                                                                      */
/* En entrée : Le nombre d'expériences nb_experiments                                                   */
/*                                                                                                      */
/* En sortie : Un tableau de nombres réels donnant la répartition dans chaque classe en pourcentages    */
/* ---------------------------------------------------------------------------------------------------- */

double * get_simulation_ABC(int nb_experiments)
{
    int simu[3] = {0, 0, 0};

    // On récupère ensuite les résultats des expériences

    int result_exp;

    for (int e = 0; e < nb_experiments; ++e) {
        result_exp = simulate_ABC();
        ++simu[result_exp];
    }

    // On calcule enfin la répartition des résultats

    double * prob_simu = malloc(3 * sizeof(double));

    if (prob_simu) {
        for (int i = 0; i < 3; ++i) {
            prob_simu[i] = ((double) simu[i] / nb_experiments) * 100;
        }
    }


    return prob_simu;
}

/* -------------------------------------------------------------------------------- */
/* simulate     Simule une expérience selon un tableau de probabilités cumulées     */
/*                                                                                  */
/* En entrée : Un tableau de probabilités cumulées cdf_array                        */
/*                                                                                  */
/* En sortie : Une classe 0, 1, 2 générée selon la loi de distribution              */
/* -------------------------------------------------------------------------------- */

int simulate(double * cdf_array)
{
    // On génère un nombre pseudo-aléatoire à l'aide de Mersenne Twister
    double psr = genrand_real1();

    // On retrouve ensuite sa classe à l'aide du tableau des probabilités cumulées
    int class = 0;
    while (psr >= cdf_array[class]) { // Cette boucle s'arrête car psr est compris entre 0 et 1
        ++class;
    }

    return class;
}

/* ---------------------------------------------------------------------------------------------------- */
/* get_simulation   Simule plusieurs expériences selon un tableau de probabilités cumulées              */
/*                  et renvoie la répartition obtenue                                                   */
/*                                                                                                      */
/* En entrée : Le nombre de classes nb_classes, Le tableau de probabilités cumulées cdf_array et        */
/*             le nombre d'expériences nb_experiments                                                   */
/*                                                                                                      */
/* En sortie : Un tableau de nombres réels donnant la répartition dans chaque classe en pourcentages    */
/* ---------------------------------------------------------------------------------------------------- */

double * get_simulation(int nb_classes, double * cdf_array, int nb_experiments)
{
    int * simu = malloc(nb_classes * sizeof(int));
    double * prob_simu;

    if (simu) {
        // Initialisation
        for (int i = 0; i < nb_classes; ++i) {
            simu[i] = 0;
        }

        // On récupère ensuite les résultats des expériences

        int result_exp;

        for (int e = 0; e < nb_experiments; ++e) {
            result_exp = simulate(cdf_array);
            ++simu[result_exp];
        }

        // On calcule enfin la répartition des résultats

        prob_simu = malloc(nb_classes * sizeof(double));

        for (int i = 0; i < nb_classes; ++i) {
            prob_simu[i] = ((double) simu[i] / nb_experiments) * 100;
        }

        free(simu);
    }

    return prob_simu;
}

/* ------------------------------------------------------------------------------------ */
/* simulate     Affiche le contenu des deux tableaux de probabilités (en pourcentages)  */
/*                                                                                      */
/* En entrée : Le nombre de classes nb_classes,                                         */
/*             un tableau de probabilités df_array (en %) et                            */
/*             un tableau de probabilités cumulées cdf_array (en %)                     */
/* ------------------------------------------------------------------------------------ */

void display_probabilities(int nb_classes, double * df_array, double * cdf_array)
{
    if (df_array && cdf_array) {
        printf("Tableaux des probabilités :\n");

        for (int i = 0; i < nb_classes; ++i) {
            printf("%d - %f %% - %f %% \n", i, df_array[i], cdf_array[i]);
        }
    }
}

/* ---------------------------------------------------------------------------------------------------- */
/* simulate     Affiche le contenu d'un tableau de répartition issu d'une simulation (en pourcentages)  */
/*                                                                                                      */
/* En entrée : Le nombre de classes nb_classes,                                                         */
/*             un tableau de probabilités df_array (en %) et                                            */
/*             le nombre d'expériences nb_experiments                                                    */
/* ---------------------------------------------------------------------------------------------------- */

void display_simulation(int nb_classes, double * df_array, int nb_experiments)
{
    if (df_array) {
        printf("Résultats de la simulation pour %d expériences :\n", nb_experiments);

        for (int i = 0; i < nb_classes; ++i) {
            printf("%d - %f %% \n", i, df_array[i]);
        }
    }
}



int main(void)
{
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    // On utilise d'abord la loi de distribution donnée dans l'énoncé

    printf("--- Première loi de distribution discrète : A, B et C ---\n");

    double * prob_simu;

    prob_simu = get_simulation_ABC(1000);
    display_simulation(3, prob_simu, 1000);
    free(prob_simu);

    prob_simu = get_simulation_ABC(10000);
    display_simulation(3, prob_simu, 10000);
    free(prob_simu);

    prob_simu = get_simulation_ABC(100000);
    display_simulation(3, prob_simu, 100000);
    free(prob_simu);

    prob_simu = get_simulation_ABC(1000000);
    display_simulation(3, prob_simu, 1000000);
    free(prob_simu);

    // On utilise ensuite l'exemple donné dans le cours
    // (Taux de cholestérol HDL en g/L)

    printf("--- Autre loi de distribution discrète (Cholestérol HDL) ---\n");

    int nb_classes = 6;

    int obs[6] = { 100, 400, 600, 400, 100, 200 };

    double * proba = get_distribution_function(nb_classes, obs);
    double * cumul_proba = get_cumulative_density_function(nb_classes, proba);

    double * proba_p = get_percentages(nb_classes, proba);
    double * cumul_proba_p = get_percentages(nb_classes, cumul_proba);
    display_probabilities(nb_classes, proba_p, cumul_proba_p);
    free(proba_p);
    free(cumul_proba_p);

    prob_simu = get_simulation(nb_classes, cumul_proba, 1000);
    display_simulation(nb_classes, prob_simu, 1000);
    free(prob_simu);

    prob_simu = get_simulation(nb_classes, cumul_proba, 1000000);
    display_simulation(nb_classes, prob_simu, 1000000);
    free(prob_simu);

    free(cumul_proba);
    free(proba);

    return 0;
}
