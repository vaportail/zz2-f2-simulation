/* 
   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.

   Before using, initialize the state by using init_genrand(seed)  
   or init_by_array(init_key, key_length).

   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.                          

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote 
        products derived from this software without specific prior written 
        permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


   Any feedback is very welcome.
   http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Period parameters */  
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */

static unsigned long mt[N]; /* the array for the state vector  */
static int mti=N+1; /* mti==N+1 means mt[N] is not initialized */

/* initializes mt[N] with a seed */
void init_genrand(unsigned long s)
{
    mt[0]= s & 0xffffffffUL;
    for (mti=1; mti<N; mti++) {
        mt[mti] = 
	    (1812433253UL * (mt[mti-1] ^ (mt[mti-1] >> 30)) + mti); 
        /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
        /* In the previous versions, MSBs of the seed affect   */
        /* only MSBs of the array mt[].                        */
        /* 2002/01/09 modified by Makoto Matsumoto             */
        mt[mti] &= 0xffffffffUL;
        /* for >32 bit machines */
    }
}

/* initialize by an array with array-length */
/* init_key is the array for initializing keys */
/* key_length is its length */
/* slight change for C++, 2004/2/26 */
void init_by_array(unsigned long init_key[], int key_length)
{
    int i, j, k;
    init_genrand(19650218UL);
    i=1; j=0;
    k = (N>key_length ? N : key_length);
    for (; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1664525UL))
          + init_key[j] + j; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++; j++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
        if (j>=key_length) j=0;
    }
    for (k=N-1; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1566083941UL))
          - i; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
    }

    mt[0] = 0x80000000UL; /* MSB is 1; assuring non-zero initial array */ 
}

/* generates a random number on [0,0xffffffff]-interval */
unsigned long genrand_int32(void)
{
    unsigned long y;
    static unsigned long mag01[2]={0x0UL, MATRIX_A};
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (mti >= N) { /* generate N words at one time */
        int kk;

        if (mti == N+1)   /* if init_genrand() has not been called, */
            init_genrand(5489UL); /* a default initial seed is used */

        for (kk=0;kk<N-M;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        for (;kk<N-1;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK);
        mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1UL];

        mti = 0;
    }
  
    y = mt[mti++];

    /* Tempering */
    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680UL;
    y ^= (y << 15) & 0xefc60000UL;
    y ^= (y >> 18);

    return y;
}

/* generates a random number on [0,0x7fffffff]-interval */
long genrand_int31(void)
{
    return (long)(genrand_int32()>>1);
}

/* generates a random number on [0,1]-real-interval */
double genrand_real1(void)
{
    return genrand_int32()*(1.0/4294967295.0); 
    /* divided by 2^32-1 */ 
}

/* generates a random number on [0,1)-real-interval */
double genrand_real2(void)
{
    return genrand_int32()*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on (0,1)-real-interval */
double genrand_real3(void)
{
    return (((double)genrand_int32()) + 0.5)*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on [0,1) with 53-bit resolution*/
double genrand_res53(void) 
{ 
    unsigned long a=genrand_int32()>>5, b=genrand_int32()>>6; 
    return(a*67108864.0+b)*(1.0/9007199254740992.0); 
} 
/* These real versions are due to Isaku Wada, 2002/01/09 added */


////// Fonctions écrites pour le TP //////

/* ------------------------------------------------------------------------------------ */
/* rejection      Implémente l'algorithme général de rejet                              */
/*                                                                                      */
/* En entrée : Un fonction de densité f, des bornes min_x, max_x, min_y et max_y        */
/*                                                                                      */
/* En sortie : Une valeur réelle pseudo-aléatoire qui suit la fonction de densité f     */
/* ------------------------------------------------------------------------------------ */

double rejection(double (* f)(double x), double min_x, double max_x, double min_y, double max_y)
{
    double na_1, na_2, x, y;

    do {
        na_1 = genrand_real1();
        na_2 = genrand_real1();

        x = min_x + na_1 * (max_x - min_x);
        y = max_y * na_2;
    } while (y > f(x));

    return x;
}

/* ---------------------------------------------------------------------------- */
/* uniform      Génére un nombre aléatoire compris dans un certain intervalle   */
/*                                                                              */
/* En entrée : Des flottants à double précision a et b                          */
/*                                                                              */
/* En sortie : Un nombre réel aléatoire compris dans l'intervalle [a,b]         */
/* ---------------------------------------------------------------------------- */

double uniform(double a, double b)
{
    // We first generate a random number on [0,1]-real-interval
    return a + genrand_real1() * (b - a);
}

/* ------------------------------------------------------------------------------------ */
/* rejection      Simule 40 lancers de dés et donne la somme des résultats              */
/*                                                                                      */
/* En sortie : La somme des résultats de 40 lancers d'un dé à 6 faces                   */
/* ------------------------------------------------------------------------------------ */

double dice_40_times()
{
    int sum = 0;
    int dice_result;

    for (int i = 0; i < 40; ++i) {

        dice_result = (int) uniform(0, 6) + 1; // On veut une valeur entre 1 et 6
        sum += dice_result;
    }

    return sum;
}

/* ---------------------------------------------------------------------------------------- */
/* dice_distribution        Donne la répartition des résultats issus de 40 lancers d'un dé  */
/*                                                                                          */
/* En entrée : Le nombre d'expériences nb_experiments                                       */
/*                                                                                          */
/* En sortie : Un tableau d'entiers indiquant le nombre d'observations pour chaque valeur   */
/* ---------------------------------------------------------------------------------------- */

int * dice_distribution(int nb_experiments) {
    int nb_classes = 241;

    int * dist = malloc(nb_classes * sizeof(int));

    if (dist) {
        // Initialisation

        for (int i = 0; i < nb_classes; ++i) {
            dist[i] = 0;
        }

        for (int k = 0; k < nb_experiments; ++k) {
            dist[(int) dice_40_times()]++;
        }
    }

    return dist;
}

/* ------------------------------------------------------------------------------------------------ */
/* avg_distribution        Donne la moyenne théorique des résultats issus de plusieurs expériences  */
/*                                                                                                  */
/* En entrée : Le nombre d'expériences nb_experiments et une expérience pseudo-aléatoire f          */
/*                                                                                                  */
/* En sortie : La moyenne théorique des résultats de toutes les expériences                         */
/* ------------------------------------------------------------------------------------------------ */

double avg_distribution(int nb_experiments, double (* f)())
{
    double sum = 0;

    for (int i = 0; i < nb_experiments; ++i) {
        sum += f();
    }

    return sum / nb_experiments;
}

/* -------------------------------------------------------------------------------------------------------- */
/* gaussian_distribution        Donne la répartition des résultats suivant une loi normale centrée réduite  */
/*                                                                                                          */
/* En entrée : Le nombre d'expériences nb_experiments, des bornes min et max,                               */
/*             et le nombre de classes voulues dans le tableau de répartition                               */
/*                                                                                                          */
/* En sortie : Un tableau d'entiers indiquant le nombre d'observations pour chaque valeur                   */
/* -------------------------------------------------------------------------------------------------------- */

int * gaussian_distribution(int nb_experiments, double min, double max, int nb_classes) {
    int * dist = malloc(nb_classes * sizeof(int));

    double r1, r2, x1, x2;
    int c1, c2;

    if (dist) {
        // Initialisation

        for (int i = 0; i < nb_classes; ++i) {
            dist[i] = 0;
        }

        // Simulation

        for (int k = 0; k < nb_experiments; ++k) {
            // Calcul des valeurs aléatoires

            r1 = genrand_real1();
            r2 = genrand_real1();

            x1 = cos(2 * M_PI * r2) * sqrt(-2 * log(r1));
            x2 = sin(2 * M_PI * r2) * sqrt(-2 * log(r1));

            if (x1 >= min && x1 <= max) {
                c1 = (x1 - min) / (max - min) * nb_classes;
                dist[c1]++;
            }

            if (x2 >= min && x2 <= max) {
                c2 = (x2 - min) / (max - min) * nb_classes;
                dist[c2]++;
            }
        }
    }

    return dist;
}

/* ---------------------------------------------------------------------------------------- */
/* export_distribution        Ecrit le contenu d'un tableau de répartition dans un flux     */
/*                                                                                          */
/* En entrée : Un flux f, un tableau de répartition dist, des bornes min et max,            */
/*             et le nombre de classes voulues dans le tableau de répartition               */
/*                                                                                          */
/* En sortie : Un tableau d'entiers indiquant le nombre d'observations pour chaque valeur   */
/* ---------------------------------------------------------------------------------------- */

void export_distribution(FILE * f, int * dist, double min, double max, int nb_classes) {
    double size_class = (max - min) / nb_classes;

    for (int i = 0; i < nb_classes; ++i) {
        fprintf(f, "%lf,%d\n", min + i * size_class, dist[i]);
    }
}



int main(void)
{
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    int nb_experiments;

    FILE * dist_csv;
    int * dist;

    // Calcul de la moyenne empirique de cette loi de distibution

    nb_experiments = 1000000;

    double avg = avg_distribution(nb_experiments, dice_40_times);

    printf("Moyenne du résultat du lancer de 40 dés pour %d tirages : %lf\n", nb_experiments, avg);

    // Distribution de la loi de répartition
    // Exportée dans un fichier .csv afin d'en faciliter l'exploitation ultérieure

    char dist_dice[] = "dice_40_dist.csv";

    dist_csv = fopen(dist_dice, "w");

    if (dist_csv) {
        dist = dice_distribution(nb_experiments);

        for (int i = 0; i < 241; ++i) {
            fprintf(dist_csv, "%d,%d\n", i, dist[i]);
        }

        printf("Données enregistrées dans le fichier %s !\n", dist_dice);
        fclose(dist_csv); // Les données pourront ensuite être exploitées

        free(dist);
    }

    // Cas de la loi normale centrée réduite

    char dist_gaussian[2][32] = { "gaussian_dist_1000.csv", "gaussian_dist_1000000.csv" };

    double min = -5, max = 5;
    int nb_classes = 20;

    // 1000 expériences

    nb_experiments = 1000;

    dist_csv = fopen(dist_gaussian[0], "w");

    if (dist_csv) {
        dist = gaussian_distribution(nb_experiments, min, max, nb_classes);

        export_distribution(dist_csv, dist, min, max, nb_classes);

        printf("Données de la loi gaussienne enregistrées dans le fichier %s !\n", dist_gaussian[0]);

        free(dist);
        fclose(dist_csv);
    }

    // 1000000 expériences

    nb_experiments = 1000000;

    dist_csv = fopen(dist_gaussian[1], "w");

    if (dist_csv) {
        dist = gaussian_distribution(nb_experiments, min, max, nb_classes);

        export_distribution(dist_csv, dist, min, max, nb_classes);

        printf("Données de la loi gaussienne enregistrées dans le fichier %s !\n", dist_gaussian[1]);

        free(dist);
        fclose(dist_csv);
    }

    return 0;
}
