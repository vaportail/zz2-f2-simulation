\documentclass[french]{article}

\usepackage[utf8]{inputenc}
\usepackage{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{graphicx}

\usepackage[margin=3cm]{geometry}

\usepackage{mathtools}
\usepackage{amssymb}

\usepackage{hyperref}

\usepackage{listings}
\usepackage{algorithm2e}

\setlength{\parskip}{1ex plus 0.5ex minus 0.2ex}

\title{TP 2 - Génération de variables aléatoires}
\author{Valentin PORTAIL, \emph{Enseignant :} David HILL}
\date{7 octobre 2024}

\begin{document}

\begin{titlepage}
    \centering
    
    \maketitle
    
    \begin{figure*}[ht]
        \centering
        \includegraphics[width=0.8\linewidth]{6sided_dice.jpg}
    \end{figure*}
\end{titlepage}

\pagebreak

\tableofcontents
\listoffigures

\pagebreak

\section{Introduction}

Dans le TP N°1, nous avons vu comment implémenter plusieurs générateurs de nombres pseudo-aléatoires en C :

\begin{description}
    \item[Un générateur non-linéaire de Von Neumann] Il est très simple à implémenter, mais sa période est très courte et il peut très vite tomber dans un cycle ou converger vers une certaine valeur s'il n'a pas correctement été initialisé avec une graine de qualité.
    \item[Le générateur de nombres pseudo-aléatoires rand()] Il est présent dans les bibliothèques par défaut en C. Sa période est très courte et il est donc fortement déconseillé dans un cadre scientifique.
    \item[Un générateur congruentiel linéaire (GCL)] C'est un générateur très rapide, au résultat répétable et prévisible, mais qui possède de nombreux défauts d'un point de vue structurel et statistique. Son utilisation est donc à éviter dans des programmes scientifiques, mais il peut éventuellement être utilisé dans les jeux.
\end{description}

Dans un contexte scientifique, ces générateurs sont donc à éviter. Dans la pratique, il nous faut donc un générateur de nombres pseudo-aléatoires, afin d'avoir un résultat répétable, ayant une période suffisamment grande et des résultats uniformément répartis dans un nombre important de dimensions.

Dans ce TP, nous découvrirons le générateur de nombres pseudo-aléatoires \emph{Mersenne Twister}, puis nous l'utiliserons afin de modéliser plusieurs lois de distribution.

\section{Mersenne Twister}

Dans ce TP, nous utiliserons le générateur de nombres pseudo-aléatoires \emph{Mersenne Twister}. Développé en 1997 par Makoto Matsumoto et Takuji Nishimura, ce générateur est l'un des plus performants et est très utilisé pour les simulations informatiques. 
Il possède une période de $2^{19937} - 1$ et est uniformément réparti sur un grand nombre de dimensions, ce qui permet d'avoir des résultats de grande qualité.
En revanche, son utilisation est déconseillée pour de la cryptographie.

Nous utiliserons une implémentation à 32 bits sortie en 2002 et disponible sur le site de Makoto Matsumoto : \url{http://www.math.sci.hiroshima-u.ac.jp/m-mat/MT/MT2002/emt19937ar.html}.

L'archive, au format \texttt{.tgz}, contient les fichiers suivants :
\begin{description}
	\item[mt19937ar.c] Le code source écrit en C ;
	\item[mt19937ar.out] La sortie attendue ;
	\item[readme-mt.txt] Description du fonctionnement du programme et mentions légales
\end{description}

Vérifions d'abord si le programme C renvoie bien le résultat attendu. On commence par extraire l'archive et récupérer les différents fichiers. En compilant le programme fourni et en l'exécutant, on obtient bien les mêmes résultats que ceux attendus. On peut le vérifier en enregistrant le résultat obtenu dans un fichier, puis en utilisant la commande Bash suivante :

\begin{lstlisting}
$ diff mt19937ar.out nom_du_fichier
\end{lstlisting}

Si tout se passe bien, la commande ne devrait rien retourner. Cela signifie que les deux fichiers ont exactement le même contenu. L'intérêt est d'avoir un résultat prévisible et reproductible, ce qui est crucial dans un contexte scientifique où chaque expérience doit pouvoir être facilement reproduite et donner les mêmes résultats.

Pour la suite du TP, nous modifierons le code fourni par Makoto Matsumoto en implémentant de nouvelles fonctions afin de pouvoir répondre aux différentes questions.

\section{Génération de nombres aléatoires uniformes entre A et B}

Notre objectif est d'écrire une fonction \verb+uniform+ qui prendra en paramètres deux nombres réels \verb+a+ et \verb+b+ et qui générera des nombres pseudo-aléatoires compris dans l'intervalle $[a,b]$.

Pour cela, nous utiliserons la fonction  \verb+genrand_real1+, implémentée dans le programme fourni et qui renvoie un nombre réel $x$ compris dans l'intervalle $[0,1]$. Notre objectif est d'obtenir, à partir de $x$, une nouvelle valeur $y$ telle que $y \in [a, b], (a, b) \in \mathbb{R}^2$.

On commence par poser $z$ tel que $z = x (b-a)$. On a donc $z \in [0, b-a]$.
On pose enfin $y = z + a$. On a $y \in [a, b]$, et on a donc réussi à obtenir une valeur pseudo-aléatoire dans l'intervalle $[a,b]$ à partir d'une autre valeur pseudo-aléatoire dans l'intervalle $[0,1]$.

Le code de cette section se trouve dans le fichier \verb+mt_uniform.c+. Le programme principal présent dans la fonction \verb+main+ permet de générer de manière pseudo-aléatoire des températures comprises entre -98°C et 57,7°C avec une décimale de précision. Pour cela, il suffit simplement de tronquer la valeur obtenue par la fonction \verb+uniform+ pour ne garder que les décimales qui nous intéressent. Voici un échantillon des 100 premières températures générées :

\begin{verbatim}
100 outputs of temperatures between -98°C and 57.7°C
using uniform(a, b) and genrand_real1()
     -59.3      -63.3      -80.7       50.9       55.3 
      23.2       23.6      -89.7      -68.6       -4.1 
      -5.2       19.6      -74.6      -44.4       55.9 
     -56.6       42.1       18.8      -33.7      -56.7 
     -46.5      -52.0        5.7      -34.0       42.5 
      45.7      -95.4      -11.6       -3.7       19.4 
       3.2      -71.7      -76.0      -88.2      -72.1 
      21.6      -50.0       55.3      -47.4       37.4 
     -21.8       37.2        9.0      -37.8        7.0 
     -12.1      -61.6      -16.5      -21.5       19.4 
      50.0      -90.0       -9.0       36.5      -67.7 
     -10.6        1.2      -70.6      -19.1      -30.1 
       2.1       52.9       23.5      -91.4       20.5 
     -69.9      -17.6       10.8      -30.3      -20.7 
     -96.7      -54.8       14.5        7.7      -61.3 
     -18.8       56.3        7.1       40.4       11.9 
     -51.5      -82.8      -13.7        1.4      -51.7 
      37.7      -88.1       -2.8       -5.2       12.8 
      55.5      -53.5       32.3      -58.9      -11.1 
     -30.9       11.3      -25.5       11.0      -59.9
\end{verbatim}

\section{Reproduction de lois de distribution empiriques discrètes}

Toutes les fonctions et procédures créées pour cette partie se trouvent dans le fichier \verb+mt_discrete+, présent dans le dossier \verb+part3_discrete_empirical_distributions+.

Dans cette section, nous analyserons la répartition d'une caractéristique au sein d'une population. L'objectif est d'extraire de nos observations une loi de distribution discrète, puis de l'utiliser pour générer une nouvelle population qui suit cette loi.

Le protocole est décrit dans le cours. Il se décompose en plusieurs étapes :

\begin{enumerate}
	\item Si la variable aléatoire qui correspond à la propriété analysée est continue, la "discrétiser" en choisissant un intervalle suffisamment pertinent, puis en créant autant de classes que nécessaire. Ce choix est sujet à interprétation et peut varier considérablement en fonction de la propriété étudiée.
	\item Effectuer l'expérience un certain nombre de fois et compter le nombre d'individus qui rentrent dans chacune des classes.
	\item Si ce n'est pas déjà fait, compter le nombre total d'observations. Puis, calculer la probabilité qu'un individu de la population pris au hasard appartienne à chacune des classes. Il s'agit du nombre d'observations dans la classe divisé par le nombre total d'observations. Une fois cela fait, construire un tableau des probabilités à partir des résultats trouvés.
	\item A partir du tableau des probabilités, construire la fonction de densité cumulée. Le premier élément est égal à la première probabilité. Puis, pour $i \in [2, n]$, $n$ désignant le nombre d'éléments, le $i$-ième élément est égal à la somme des $i$ premières probabilités du précédent tableau.
	\item A l'aide de la fonction de densité cumulée et d'un générateur de nombres pseudo-aléatoires, il est possible de générer une nouvelle population qui suit la loi de distribution.
\end{enumerate}

\subsection{Premier exemple : Classes A, B et C}

Le premier exemple, donné dans le sujet de TP, est très simple. On suppose que l'on a un ensemble de données réparti dans 3 classes :

\begin{itemize}
	\item Il y a 350 éléments dans la classe A.
	\item Il y a 450 éléments dans la classe B.
	\item Il y a 200 éléments dans la classe C.
\end{itemize}

Dans ce cas précis, nous avons $350 + 450 + 200 = 1000$ observations au total. Il est donc très facile d'obtenir le tableau des probabilités ainsi que la fonction de densité cumulée :

\begin{center}
    \begin{tabular}{r|ccc|l}
    	& A & B & C & Total \\
    	Observations & 350 & 450 & 200 & 1000 \\
    	Probabilités & 35 \% & 45 \% & 20 \% & 100 \%\\
    	Probabilités cumulées & 35 \% & 80 \% & 100 \% & 100 \% \\
    \end{tabular}
\end{center}

La fonction \verb+get_simulation_ABC+ prend en paramètre le nombre d'expériences \verb+nb_experiments+ que l'on souhaite réaliser et lance la simulation en suivant cette loi de distribution. Comme elle est très simple, elle est "codée en dur" dans le programme. En toute rigueur, il faudrait également coder des fonctions pour obtenir les tableaux de probabilités simples et cumulées (cf. section \ref{cholesterol}).

En fonction du nombre d'expériences, on obtient les résultats suivants :

\begin{center}
    \begin{tabular}{r|ccc}
    	& A & B & C \\
    	1000 expériences & 34.300000 \% & 45.500000 \% & 20.200000 \% \\
    	10000 expériences & 34.450000 \% & 44.470000 \% & 20.080000 \% \\
    	100000 expériences & 34.938000 \% & 45.032000 \% & 20.030000 \% \\
    	1000000 expériences & 35.028100 \% & 44.958000 \% & 20.013900 \% \\
    \end{tabular}
\end{center}

On peut remarquer que plus il y a d'expériences, plus les valeurs empiriques se rapprochent des valeurs théoriques (35 \%, 45 \% et 20 \%). Selon la loi des grands nombres, ce comportement est cohérent et attendu.

\subsection{Exemple général : Le cholestérol HDL}
\label{cholesterol}

Dans un cas plus général, il n'est pas toujours envisageable de calculer à la main le tableau des probabilités et la fonction de densité cumulée : le nombre de classes peut être trop grand ou certains calculs peuvent donner des résultats avec un nombre trop important de décimales. Il est donc préférable de coder plusieurs fonctions qui permettent d'effectuer ces opérations. Cela permet également de rendre le programme plus polyvalent : il suffit de modifier les observations initiales et le nombre de classes pour obtenir une nouvelle loi de répartition et une nouvelle simulation. Voici les principales fonctions et procédures implémentées :

\begin{itemize}
    \item \verb+get_distribution_function+ :
    Prend en paramètres un entier \verb+nb_classes+, correspondant au nombre de classes, et un tableau d'entiers \verb+obs+, comprenant pour chaque classe le nombre d'observations.
    Calcule le nombre total d'observations et renvoie le tableau des probabilités.
    \item \verb+get_cumulative_density_function+ :
    Prend en paramètres un entier \verb+nb_classes+, correspondant au nombre de classes, et un tableau de flottants \verb|df_array|, correspondant au tableau de probabilités généré par la fonction précédente.
    Renvoie le tableau des probabilités cumulées, correspondant à la fonction de densité cumulée.
    \item \verb+get_percentages+ :
    Prend en paramètres un entier \verb+nb_classes+, représentant le nombre de classes, et un tableau de probabilités \verb+array+.
    Renvoie un nouveau tableau avec les mêmes probabilités converties en pourcentages.
    \item \verb+simulate+ :
    Prend en paramètre le tableau des probabilités cumulées \verb+cdf_array+.
    Génère un nombre pseudo-aléatoire entre 0 et 1 à l'aide de \emph{Mersenne Twister}, puis renvoie sa classe, trouvée à l'aide du tableau des probabilités cumulées.
    \item \verb+get_simulation+ :
    Prend en paramètres un entier \verb|nb_classes|, correspondant au nombre de classes,  le tableau des probabilités cumulées \verb|cdf_array| et un entier \verb|nb_experiments|, représentant le nombre d'expériences à réaliser.
    Lance \verb|nb_experiments| expériences, puis stocke les résultats obtenus dans un tableau et renvoie un tableau contenant la répartition des résultats en pourcentages.
\end{itemize}

Afin de tester ces fonctions, nous utiliserons le jeu de données présent dans le cours. On étudie le taux de cholestérol HDL en grammes par litre de sang. L'intervalle choisi est de 5 grammes par litre, ce qui nous donne 6 classes. On effectue 1800 observations réparties dans les classes de la manière suivante :

\begin{center}
    \begin{tabular}{r|cccccc}
        Taux (en g/L) & [40,45] & [45,50] & [50,55] & [55,60] & [60,65] & [65,70] \\
        \hline
        Observations & 100 & 400 & 600 & 400 & 100 & 200 \\
    \end{tabular}    
\end{center}

On commence par récupérer le tableau des probabilités et la fonction de densité cumulée, puis on effectue une simulation avec 1000, puis 1000000 expériences. On obtient les résultats suivants :

\begin{center}
    \begin{tabular}{r|cccccc}
        Numéro de classe & 0 & 1 & 2 & 3 & 4 & 5 \\
        Taux (en g/L) & [40,45] & [45,50] & [50,55] & [55,60] & [60,65] & [65,70] \\
        \hline
        Observations & 100 & 400 & 600 & 400 & 100 & 200 \\
        Probabilités (en \%) & 5.555556 & 22.222222 & 33.333333 & 22.222222 & 5.555556 & 11.111111 \\ 
        Probabilités cumulées (en \%) & 5.555556 & 27.777778 & 61.111111 & 83.333333 & 88.888889 & 100.000000\\
        \hline
        1000 expériences & 6.400000 & 21.900000 & 31.100000 & 21.500000 & 5.700000 & 13.400000 \\
        1000000 expériences & 5.561400 & 22.319500 & 33.263500 & 22.195900 & 5.581400 & 11.078300 \\
    \end{tabular}    
\end{center}

Là encore, plus on effectue d'expériences, plus les résultats empiriques se rapprochent des résultats théoriques.

\section{Reproduction de lois de distribution continues}

Toutes les fonctions et procédures créées pour cette partie se trouvent dans le fichier \verb+mt_continuous+, présent dans le dossier \verb+part4_continuous_distributions+.

Cette partie a pour objectif de reproduire certaines lois de distribution continues en inversant leur fonction de densité.

Soit $F$ la fonction de répartition d'une loi de distribution continue donnée. Si $F$ est inversible, alors en générant un nombre pseudo-aléatoire $r$ compris dans l'intervalle $[0,1]$, il est possible d'obtenir un autre nombre $x$ tel que :

\begin{equation*}
    x = F^{-1}(r)
\end{equation*}

Cette méthode est appelée \emph{anamorphose}. Elle n'est pas utilisable avec toutes les lois de distribution, mais peut s'appliquer à un grand nombre d'entre elles, comme les lois binomiales, les lois uniformes ou les lois de Weibull.

Dans cette section, nous utiliserons cette méthode pour reproduire une \emph{loi exponentielle négative}. On peut la voir comme une variante continue d'une loi de Poisson. Sa fonction de répartition est définie de la manière suivante :

\begin{equation*}
    F(x) = \int_0^x \frac{1}{M} e^{-\frac{1}{M} z} dz = 1 - e^{-\frac{1}{M} x}
\end{equation*}

Transformons cette équation pour obtenir la valeur de $x$. Soit $r \in [0,1[$ une valeur de la fonction $F$. On a :

\begin{gather*}
    r = 1 - e^{-\frac{1}{M} x} \\
    1 - r = e^{-\frac{1}{M} x} \\
    \ln(1 - r) = -\frac{1}{M} x \\
    x = -M \ln(1-r) \\
\end{gather*}

Ainsi, pour chaque valeur $r \in [0,1[$ appartenant à l'image de $F$, il est possible de trouver une unique valeur $x \in [0, +\infty[$ telle que $F(x) = r$. La fonction $F$ est donc bijective et on peut écrire $x = F^{-1}(r)$.

La fonction \verb+neg_exp+ prend en paramètre l'espérance théorique \verb+mean+ de la loi exponentielle négative souhaitée et génère un nombre pseudo-aléatoire suivant cette loi grâce à la fonction \verb+genrand_real1+ de \emph{Mersenne Twister}.

Vérifions d'abord si la moyenne empirique obtenue grâce à la simulation correspond à la moyenne théorique passée en paramètre de \verb+neg_exp+. Pour cela, nous utilisons la fonction \verb+avg_neg_exp+ qui prend en paramètre la moyenne théorique \verb+mean+ ainsi que le nombre de tirages à réaliser \verb+nb_drawings+. Elle effectue \verb+nb_drawings+ tirages et renvoie la moyenne empirique.

Enfin, il peut être utile de vérifier la répartition des différentes valeurs générées pseudo-aléatoirement selon cette loi. La fonction \verb+distribution_neg_exp+ prend en paramètres la moyenne théorique \verb|mean| et le nombre de tirages à réaliser \verb|nb_drawings|. Elle effectue autant de tirages que spécifié et renvoie un tableau de 21 cases vérifiant les règles suivantes :

\begin{itemize}
    \item Pour $i \in [0, 19[$, la $i$-ième case du tableau contient le nombre de valeurs $v$ telles que $i \leq v < i+1$.
    \item Pour $i = 20$, la vingtième case du tableau contient le nombre de valeurs $v$ supérieures ou égales à $20$.
\end{itemize}

Voici les résultats obtenus pour $1000$ et $1000000$ tirages :

\begin{center}
    \begin{tabular}{r|c|c}
         Nombre de tirages & 1000 & 1000000 \\
         \hline
         Moyenne théorique (Espérance) & 10 & 10 \\
         Moyenne empirique & 10.090822 & 10.000759 \\
         \hline
         0 & 87 & 95554 \\
         1 & 90 & 86788 \\
         2 & 72 & 77684 \\
         3 & 69 & 70698 \\
         4 & 61 & 63838 \\
         5 & 48 & 57732 \\
         6 & 56 & 51804 \\
         7 & 58 & 47393 \\
         8 & 41 & 42587 \\
         9 & 40 & 38682 \\
         10 & 36 & 34811 \\
         11 & 22 & 31613 \\
         12 & 29 & 28518 \\
         13 & 28 & 25764 \\
         14 & 23 & 23249 \\
         15 & 18 & 21286 \\
         16 & 21 & 19238 \\
         17 & 21 & 17416 \\
         18 & 17 & 15809 \\
         19 & 18 & 14266 \\
         20 et + & 145 & 135270 \\
         \hline
         Total & 1000 & 1000000 \\
    \end{tabular}
\end{center}

En recalculant la somme des valeurs présentes dans chaque classe, il est possible de vérifier si le tableau de répartition a correctement été initialisé. En temps normal, ce total doit être égal au nombre d'expériences qui ont servi à déterminer la répartition.

De plus, on peut à nouveau vérifier que la moyenne empirique correspond bien à la moyenne théorique et que plus le nombre de tirages est grand, plus ces deux moyennes sont proches.

Il est possible de retracer un histogramme à partir des résultats obtenus lors de la simulation. Celui obtenu à partir des résultats de la simulation à 1000000 expériences est en figure \ref{fig:rne}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\linewidth]{reversed_neg_exp.png}
    \caption{Répartition des résultats obtenus en simulant l'inverse d'une loi exponentielle réduite avec 1000000 tirages}
    \label{fig:rne}
\end{figure}

\section{Simulation de lois de distribution non inversibles}

Toutes les fonctions et procédures créées pour cette partie se trouvent dans le fichier \verb+mt_non_reversible+, présent dans le dossier \verb+part5_non_reversible_distributions+.

Pour certaines lois de distribution, il arrive que la fonction de répartition $F$ ne soit pas inversible. Il n'est donc pas possible d'utiliser la méthode décrite précédemment pour les simuler. 

Dans le cas général, il est possible d'utiliser une méthode dite de \emph{rejet}, qui s'inspire de la méthode de Monte-Carlo. En partant de la fonction de densité $f$ et en définissant un intervalle d'étude, il est possible de générer des valeurs pseudo-aléatoires. Elle fonctionne de la manière suivante :

\begin{algorithm}    
    Générer 2 nombres pseudo-aléatoires $r_1$ et $r_2$ entre $0$ et $1$ \;
    
    $X \leftarrow min_X + r_1 \times (max_X - min_X)$ \;
    $Y \leftarrow max_Y \times r_2$ \;

    \eSi{$Y \leq f(X)$}{
        $X$ est distribuée selon la loi donnée par la fonction de densité $f$ \;
    }{
        Recommencer : Générer à nouveau 2 nombres pseudo-aléatoires entre $0$ et $1$ \;
    }

    \caption{Algorithme général de rejet}
\end{algorithm}

La fonction \verb|rejection|, présente dans le programme C, implémente cet algorithme et prend en paramètres la fonction de densité $f$, ainsi que les bornes de la partie à étudier : $min_X$, $max_X$, $min_Y$ et $max_Y$. Toutefois, elle ne sera pas utilisée pour la suite de cette partie. En effet, nous utiliserons d'autres méthodes pour générer des nombres pseudo-aléatoires suivant des lois non-inversibles.

Dans le cas d'une \emph{loi normale}, la fonction de densité est assez facile à trouver et à étudier. Pour une loi normale centrée et réduite (i.e. d'espérance 0 et d'écart-type 1), la fonction de densité $p$ est la suivante :

\begin{equation*}
    p(x) = \frac{1}{\sqrt{2 \pi}} e^{-\frac{x^2}{2}}
\end{equation*}

\subsection{Première implémentation}

On lance 40 fois un dé ordinaire à 6 faces non pipé et on calcule la somme de tous les résultats obtenus. Le résultat final obtenu sera donc compris entre 40 (40 lancers avec la face 1) et 240 (40 lancers avec la face 6). La probabilité d'obtenir 40 ou 240 est toutefois très faible ($\frac{1}{6^{40}}$).

Notre objectif est obtenir un aperçu de la loi de répartition de cette expérience.

Plusieurs fonctions C ont été codées pour cela :

\begin{itemize}
    \item \verb|dice_40_faces| : Simule l'expérience énoncée ci-dessus de manière pseudo-aléatoire et renvoie le résultat.
    \item \verb|dice_distribution| : Prend en paramètre un nombre de tirages. Renvoie un tableau indiquant la répartition des valeurs obtenues en effectuant l'expérience décrite ci-dessus autant de fois que spécifié.
    \item \verb|avg_distribution| : Prend en paramètre un nombre de tirages et une fonction générant des nombres pseudo-aléatoires. Renvoie la moyenne des valeurs obtenues.
\end{itemize}

En exécutant le programme principal, on trouve pour 1000000 tirages une moyenne de 140.008487. La distribution de cette loi de répartition est ensuite écrite dans un fichier \verb|dice_40_dist.csv|. Cela permettra plus facilement d'exporter les résultats dans un tableau pour pouvoir les traiter. Voici ce que l'on obtient en traçant l'histogramme dans un tableur à partir des données du fichier et en se restreignant à l'intervalle $[40, 240]$ :

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\linewidth]{dist_dice_40.png}
    \caption{Répartition des résultats obtenus lors de 40 lancers d'un dé à 6 faces (avec 1000000 tirages)}
    \label{fig:dist_dice}
\end{figure}

On voit apparaître une courbe en cloche. On peu donc supposer que la loi étudiée est très proche d'une loi dite normale ou gaussienne. Dans la pratique, il serait ainsi possible d'utiliser une loi normale pour approcher les résultat de la loi donnée.

\subsection{Test d'un modèle analytique de la loi gaussienne}

Dans ce cas, nous voulons générer des nombres autour de 0 qui suivent une loi normale centrée réduite, i.e. avec une espérance de 0 et un écart-type de 1. Pour cela, nous utiliserons les fonctions de Box et Muller. Elles prennent en paramètres deux nombres aléatoires $r_1$ et $r_2$, et permettent d'obtenir deux nombres pseudo-aléatoires $x_1$ et $x_2$ qui suivent une loi normale centrée et réduite. On a :

\begin{gather*}
    x_1 = \cos(2 \pi r_2) (-2 \ln{r_1})^{\frac{1}{2}} \\
    x_2 = \sin(2 \pi r_2) (-2 \ln{r_1})^{\frac{1}{2}}
\end{gather*}

Cette fois-ci, nous implémenterons deux fonctions en C :

\begin{itemize}
    \item \verb|gaussian_distribution|, qui prend en paramètre le nombre d'expériences, une borne minimale, une borne maximale et le nombre de classes voulues. Elle construit le tableau de répartition des observations.
    \item \verb|export_distribution|, qui prend en paramètre un flux, un tableau de répartition, une borne minimale, une borne maximale et le nombre de classes voulues. Elle exporte le tableau de répartition dans le flux.
\end{itemize}

Nous effectuons la simulation d'abord avec 1000 expériences, puis avec 1000000. On stocke les résultats dans deux fichiers respectivement nommés \verb|gaussian_dist_1000.csv| et \verb|gaussian_dist_1000000.csv|. Nous traçons ensuite les histogrammes de ces valeurs générées grâce à un tableur. Ces histogrammes sont visibles en figures \ref{fig:dist_gauss_1000} et \ref{fig:dist_gauss_1000000}

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\linewidth]{dist_gauss_1000.png}
    \caption{Répartition des résultats obtenus lors de la simulation d'une loi normale centrée réduite (avec 1000 expériences)}
    \label{fig:dist_gauss_1000}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\linewidth]{dist_gauss_1000000.png}
    \caption{Répartition des résultats obtenus lors de la simulation d'une loi normale centrée réduite (avec 1000000 expériences)}
    \label{fig:dist_gauss_1000000}
\end{figure}

Ici encore, on peut voir apparaître un motif "en cloche". Cela est pertinent, nous travaillons en effet avec une loi normale. On remarque aussi que la majorité des résultats sont très proches de 0, l'espérance théorique d'une loi normale centrée réduite. Certains résultats sont plus éloignés, mais ils sont majoritairement compris entre -1 et 1. 

Encore une fois, plus le nombre d'expériences est grand, plus l'histogramme obtenu se rapproche de la courbe théorique.

\section{Ouverture : Dans d'autres langages de programmation}

Dans ce TP, nous avons utilisé le langage C accompagné du programme \emph{Mersenne Twister} de Makoto Matsumoto. Cependant, il existe de nombreuses bibliothèques dans différents langages de programmation pour générer des variables aléatoires :

\begin{itemize}
    \item En C++, la bibliothèque \verb|random| définit de nombreuses fonctions permettant de générer des nombres aléatoires de manière uniforme. Elle implémente par défaut de nombreuses lois de distribution (loi uniforme, loi exponentielle, loi de Poisson, loi normale \ldots), ainsi que plusieurs générateurs de nombres pseudo-aléatoires, dont \emph{Mersenne Twister}.
    \item Par défaut, en Java, le générateur de nombres pseudo-aléatoires présent dans la classe \verb|Random| n'est pas adapté au calcul scientifique. En effet, elle utilise une formule de congruence linéaire (cf. TP1). Des bibliothèques implémentent, quant à elles, des générateurs de nombres pseudo-aléatoires pouvant être utilisés dans la cadre. C'est le cas, par exemple, de la bibliothèque \verb|Apache Commons Mathematics|.
\end{itemize}

\section{Conclusion}

Nous avons donc vu différentes stratégies qui permettent, à partir d'un bon générateur de nombres pseudo-aléatoires, de simuler des lois de distribution données :

\begin{itemize}
    \item Les lois uniformes s'obtiennent de manière assez simple. A l'aide d'une fonction affine, il est possible de passer d'une distribution sur l'intervalle $[0,1]$ à une distribution sur l'intervalle $[a,b], (a,b) \in \mathbb{R}^2$. Si $x \in [0,1]$, alors en prenant une fonction $f$ telle que $f(x) = a + x(b-a)$, on a : $f(x) \in [a,b]$.
    \item Pour obtenir une loi de distribution discrète, on part d'un échantillon initial que l'on a réparti dans différentes classes. On calcule les probabilités qu'un élément de l'échantillon pris au hasard appartienne à chacune des classes, puis on construit le tableau des probabilités cumulées. On se sert ensuite de ce tableau pour simuler une nouvelle population.
    \item Certaines lois de distribution continues sont inversibles (loi binomiale, loi uniforme, loi exponentielle négative \ldots). On note $F$ la fonction de répartition d'une telle loi. Dans le cas, il suffit de générer un nombre pseudo-aléatoire $r$ compris entre $0$ et $1$. On pose ensuite $x = F^{-1}(r)$. $x$ est donc réparti selon la loi de distribution décrite par $F$.
    \item Dans le cas où la fonction de répartition $F$ d'une loi de distribution continue n'est pas inversible, il est possible d'utiliser la méthode de \emph{rejet}, basée sur la méthode de \emph{Monte-Carlo}, pour obtenir des nombres pseudo-aléatoires qui suivent la loi voulue.
\end{itemize}

Ces stratégies nous seront très utiles en simulation. En effet, il est souvent demandé de simuler certaines lois de distribution en générant des nombres pseudo-aléatoires. Les propriétés de répétabilité et la grande qualité du générateur \emph{Mersenne Twister} nous permettent d'obtenir des résultats fiables qui pourront nous donner une idée globale de la répartition de la loi étudiée.

\end{document}