\babel@toc {french}{}\relax 
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}%
\contentsline {section}{\numberline {2}Mersenne Twister}{2}{section.2}%
\contentsline {section}{\numberline {3}Génération de nombres aléatoires uniformes entre A et B}{3}{section.3}%
\contentsline {section}{\numberline {4}Reproduction de lois de distribution empiriques discrètes}{3}{section.4}%
\contentsline {subsection}{\numberline {4.1}Premier exemple : Classes A, B et C}{4}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Exemple général : Le cholestérol HDL}{5}{subsection.4.2}%
\contentsline {section}{\numberline {5}Reproduction de lois de distribution continues}{6}{section.5}%
\contentsline {section}{\numberline {6}Simulation de lois de distribution non inversibles}{7}{section.6}%
\contentsline {subsection}{\numberline {6.1}Première implémentation}{8}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Test d'un modèle analytique de la loi gaussienne}{9}{subsection.6.2}%
\contentsline {section}{\numberline {7}Ouverture : Dans d'autres langages de programmation}{10}{section.7}%
\contentsline {section}{\numberline {8}Conclusion}{11}{section.8}%
