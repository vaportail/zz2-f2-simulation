#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "mt19937ar.h"

/* ------------------------------------------------------------------------------------ */
/* sim_pi   Calcule une estimation de la valeur de Pi avec la méthode de Monte Carlo    */
/*                                                                                      */
/* En entrée : Le nombre de points nb_points voulu                                      */
/*                                                                                      */
/* En sortie : Une estimation de la valeur de Pi.                                       */
/* ------------------------------------------------------------------------------------ */

double sim_pi(int nb_points)
{
    int in_circle = 0;

    double x_r, y_r;

    for (int i = 0; i < nb_points; i++) {
        // On génère un point aléatoire dans [0,1]²

        x_r = genrand_real1();
        y_r = genrand_real1();

        // Si le point fait partie du disque de centre (0,0) et de rayon 1, on le compte.

        if ((x_r * x_r + y_r * y_r) <= 1) {
            in_circle++;
        }
    }

    /* L'aire totale du disque de centre (0,0) et de rayon 1 est de pi*1² = pi.
        Tous les points du disque sont compris dans la portion de plan [-1,1]².
        En générant des points dans [0,1]², on a une estimation de pi/4.
        Il suffit donc de multiplier par 4 pour trouver
        une estimation de pi. */

    return 4 * (double) in_circle / nb_points;
}

/* -------------------------------------------------------------------------------------------- */
/* sim_pi_n   Calcule plusieurs estimations de la valeur de Pi avec la méthode de Monte Carlo   */
/*                                                                                              */
/* En entrée : Le nombre de points nb_points et le nombre d'expériences nb_experiences          */
/*                                                                                              */
/* En sortie : Un tableau contenant nb_experiences estimations de la valeur de Pi.              */
/* -------------------------------------------------------------------------------------------- */

double * sim_pi_n(int nb_points, int nb_experiments)
{
    // On stocke les résultats des expériences dans un tableau
    double * results = malloc(nb_experiments * sizeof(double));

    if (results) {
        // On effectue l'expérience j fois

        for (int k = 0; k < nb_experiments; k++) {
            results[k] = sim_pi(nb_points);
        }
    }

    return results;
}

/* ------------------------------------------------------------------------------------ */
/* mean_tab   Calcule la moyenne des valeurs d'un tableau                               */
/*                                                                                      */
/* En entrée : Un tableau de nombres flottants t et la taille size du tableau           */
/*                                                                                      */
/* En sortie : La moyenne des valeurs du tableau t                                      */
/* ------------------------------------------------------------------------------------ */

double mean_tab(double * t, int size)
{
    double sum = 0;

    for (int k = 0; k < size; k++) {
        sum += t[k];
    }

    return sum / size;
}

/* -------------------------------------------------------------------------------------------- */
/* quantile_student   Renvoie les quantiles d'une loi de Student                                */
/*                                                                                              */
/* En entrée : Une valeur entière n                                                             */
/*                                                                                              */
/* En sortie : Le quantile de la loi de Student de degré n-1 nécessaire pour calculer un        */
/*             intervalle de confiance à 95 %                                                   */
/* -------------------------------------------------------------------------------------------- */

double quantile_student(int n)
{
    double quantile = 1.960;

    double quantiles[30] = { 12.706, 4.303, 3.182, 2.776, 2.571,
                             2.447, 2.365, 2.308, 2.262, 2.228,
                             2.201, 2.179, 2.160, 2.145, 2.131,
                             2.120, 2.110, 2.101, 2.093, 2.086,
                             2.080, 2.074, 2.069, 2.064, 2.060,
                             2.056, 2.052, 2.048, 2.045, 2.042 };

    if (n > 0) {
        if (n <= 30) {
            quantile = quantiles[n-1];
        } else if (n <= 40) {
            quantile = 2.042 + (2.021 - 2.042) / (n - 30);
        } else if (n <= 80) {
            quantile = 2.021 + (2.000 - 2.021) / (n - 40);
        } else if (n <= 120) {
            quantile = 2.000 + (1.980 - 2.000) / (n - 40);
        }
    }

    return quantile;
}

/* ------------------------------------------------------------------------------------------------ */
/* confidence_interval      Calcule l'intervalle de confiance à 95 % autour de la moyenne empirique */
/*                          d'un tableau                                                            */
/*                                                                                                  */
/* En entrée : Un tableau de nombres flottants results et un nombre d'expériences nb_experiments    */
/*                                                                                                  */
/* En sortie : Un tableau contenant les 2 bornes de l'intervalle de confiance souhaité              */
/* ------------------------------------------------------------------------------------------------ */

double * confidence_interval(double * results, int nb_experiments)
{
    // Il faut d'abord calculer la moyenne empirique

    double mean_pi = mean_tab(results, nb_experiments);

    // On calcule ensuite la variance empirique
    // à l'aide de l'estimateur non biaisé

    double sum_deviations = 0;

    for (int i = 0; i < nb_experiments; i++) {
        sum_deviations += (results[i] - mean_pi) * (results[i] - mean_pi);
    }

    double unbiased_variance = sum_deviations / (nb_experiments - 1);

    // On calcule enfin les 2 bornes de l'intervalle de confiance à 95 %

    double * bounds = malloc(2 * sizeof(double));

    double r = quantile_student(nb_experiments) * sqrt(unbiased_variance / nb_experiments);

    bounds[0] = mean_pi - r;
    bounds[1] = mean_pi + r;

    return bounds;
}



int main(void)
{
    // Initialisation de Mersenne Twister

    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    // Initialisation des variables utiles pour les expériences

    int nb_points[3] = { 1000, 1000000, 1000000000 };
    //int nb_points[7] = { 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000 };

    int nb_experiments;

    double * results;
    double mean_pi;
    double * bounds;

    clock_t start, end;
    double exec_time;

    printf("----- Estimation de la valeur de Pi par la méthode de Monte Carlo : -----\n");

    for (int i = 0; i < 3; i++) {
        start = clock(); // Permet d'obtenir le nombre de "ticks" processeur nécessaire, et non le temps complet d'exécution
        mean_pi = sim_pi(nb_points[i]);
        end = clock();

        exec_time = (double) (end - start) / CLOCKS_PER_SEC;

        printf("Avec %d points : %lf\n", nb_points[i], mean_pi);
        printf("> Temps d'exécution : %lf\n", exec_time);
    }

    // Moyenne de plusieurs expériences indépendantes

    printf(">>> Indiquez le nombre d'expériences : ");
    scanf("%d", &nb_experiments);

    for (int i = 0; i < 3; i++) {
        start = clock();
        results = sim_pi_n(nb_points[i], nb_experiments);
        mean_pi = mean_tab(results, nb_experiments);
        end = clock();

        exec_time = (double) (end - start) / CLOCKS_PER_SEC;

        printf("Avec %d points et %d expériences indépendantes : %lf\n",
                nb_points[i], nb_experiments, mean_pi);
        printf("> Erreur absolue avec Pi = %lf\n", mean_pi - M_PI);
        printf("> Erreur relative avec Pi = %lf\n", mean_pi / M_PI);
        printf("> Temps d'exécution : %lf\n", exec_time);

        // Calcul de l'invervalle de confiance à 95 %

        bounds = confidence_interval(results, nb_experiments);

        printf("> Invervalle de confiance à 95 %% : [%lf , %lf]\n", bounds[0], bounds[1]);        

        free(results);
        free(bounds);
    }

    return 0;
}
