\documentclass[french]{article}

\usepackage[utf8]{inputenc}
\usepackage{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{graphicx}

\usepackage[margin=3cm]{geometry}

\usepackage{mathtools}
\usepackage{amssymb}

\usepackage{hyperref}

\usepackage{listings}
\usepackage{algorithm2e}

\setlength{\parskip}{1ex plus 0.5ex minus 0.2ex}

\title{TP 3 - Simulation de Monte Carlo et Intervalles de confiance}
\author{Valentin PORTAIL, \emph{Enseignant :} David HILL}
\date{21 octobre 2024}

\begin{document}

\thispagestyle{empty} 

\maketitle

\begin{figure*}[ht]
    \centering
    \includegraphics[width=0.8\linewidth]{Darts_in_a_dartboard.jpg}
\end{figure*}

\pagebreak

\tableofcontents
\listoffigures

\pagebreak

\section{Introduction}

Dans le TP précédent, nous avons mis en pratique le générateur de nombres pseudo-aléatoires \emph{Mersenne Twister} pour simuler des lois de répartition simples. Ce générateur de grande qualité, possédant une très grande période ($2^{19937}$) et étant uniformément réparti sur un grand nombre de dimensions, est très utilisé dans le monde scientifique pour les simulations informatiques.

Jusqu'ici, nous avons utilisé des modèles mathématiques \emph{déterministes}, où la sortie obtenue est déterminée précisément à partir de la structure de la loi de distribution et des valeurs passées en paramètres. Ils sont très rapides et précis, et doivent donc être privilégiés dès que possible. Cependant, la plupart du temps, les systèmes complexes présentent des éléments aléatoires et ne peuvent donc pas être décrits précisément par un modèle déterministe.

Pour étudier ces modèles, il est toutefois possible d'utiliser d'autres méthodes dites \emph{stochastiques}. Ainsi, la méthode de Monte Carlo, définie en 1949 par Stanislaw Ulam, John von Neumann et Nicholas Metropolis, permet d'approcher une valeur numérique en utilisant une source d'aléatoire. Cette méthode d'échantillonnage statistique aléatoire est très utilisée en physique et en informatique. Elle trouve des applications dans de nombreux domaines :

\begin{itemize}
    \item Évaluation d'intégrales de fonctions arbitraires
    \item Applications financières
    \item Entraînement des modèles d'intelligence artificielle
    \item \ldots
\end{itemize}

Dans ce TP, nous utiliserons le générateur de nombres pseudo-aléatoires \emph{Mersenne Twister} et la méthode de Monte-Carlo pour trouver une approximation de la valeur de $\pi$.

Les résultats obtenus grâce aux différentes fonctions implémentées en C sont donnés avec une précision de $10^{-6}$, c'est-à-dire, avec 6 chiffres après la virgule. On rappelle ici les premières décimales de $\pi$ :

\begin{equation*}
    \pi = 3,141592653589793\ldots
\end{equation*}

\section{Calcul de Pi avec la méthode de Monte Carlo}

Dans cette partie, nous allons implémenter une fonction qui permet, à l'aide de points aux coordonnées générées aléatoirement, d'obtenir une approximation de la valeur de $\pi$.

Nous nous plaçons dans le plan $\mathbb{R}^2$. Prenons un disque de centre $(0, 0)$ et de rayon $1$. En utilisant la formule de l'aire du disque : $A = \pi r^2$, $r$ désignant le rayon du cercle, il est facile de retrouver que l'aire du disque de rayon $r = 1$ est de $\pi$.

En générant un point aléatoire ayant deux coordonnées $x_r$ et $y_r$ comprises entre $-1$ et $1$, il est facilement possible de vérifier si le point fait partie du disque. Pour cela, il suffit d'utiliser son équation. Notons $D$ l'ensemble des points du disque. On a :

\begin{equation*}
    (x_r, y_r) \in D \iff x_r^2 + y_r^2 \leq r^2
\end{equation*}

Cependant, la plupart des générateurs de nombres pseudo-aléatoires ne renvoient que des nombres compris entre $0$ et $1$. Afin de simplifier la démarche, nous n'étudierons donc que le quart de disque compris dans $[0, 1]^2$. On obtiendra une valeur approchée de $\frac{\pi}{4}$. Il suffira ensuite de multiplier la valeur obtenue par $4$ pour obtenir une approximation de $\pi$.

Mettons en œuvre la méthode de Monte Carlo pour trouver cette valeur approchée. Nous générons $n$ points aléatoires dans $[0, 1]^2$ de manière indépendante. Notons $m$ le nombre de points appartenant à $D$. On a donc $m \leq n$. Calculons ensuite le rapport entre le nombre de points appartenant au disque et le nombre total de points, c'est-à-dire, $\frac{m}{n}$.

Plus le nombre de points $n$ sera grand, plus le rapport $\frac{m}{n}$ va converger vers $\frac{\pi}{4}$. Ainsi, en générant simplement des points de manière aléatoire, il est possible d'obtenir une approximation du nombre $\pi$.

La fonction \verb|sim_pi|, présente dans le fichier \verb|monte_carlo.c|, effectue une telle expérience en utilisant le générateur de nombres pseudo-aléatoires \emph{Mersenne Twister}.

Voici les résultats obtenus avec l'initialisation standard de \emph{Mersenne Twister} et avec un nombre croissant de points (le temps d'exécution peut varier considérablement en fonction de la machine sur laquelle le programme est exécuté et d'une exécution à l'autre) :

\begin{center}
    \begin{tabular}{r|c|c}
         Nombre de points & Estimation de $\pi$ & Temps d'exécution (en s) \\
         $1000$ ($10^3$) & 3.124000 & 0.000025 \\
         $1000000$ ($10^6$) & 3.144720 & 0.008672 \\
         $1000000000$ ($10^9$) & 3.141541 & 5.597959 \\
    \end{tabular}
\end{center}

On peut remarquer qu'avec $10^9$ points, il est possible d'obtenir une précision en dessous de $10^{-4}$. Par tâtonnement, il est possible de retrouver, en allant de puissance de 10 en puissance de 10, le nombre de tirages nécessaire pour obtenir différents degrés de précision :

\begin{center}
    \begin{tabular}{r|l}
         Précision souhaitée & Nombre d'expériences nécessaires \\
         $10^{-2}$ & $10^{5}$ (100000) \\
         $10^{-3}$ & $10^{7}$ (10000000) \\
         $10^{-4}$ & $10^{9}$ (1000000000) \\
    \end{tabular}
\end{center}

La méthode de Monte Carlo a donc une convergence lente et il faut un nombre très important d'expériences pour obtenir un résultat avec une précision très importante. Cependant, elle est très simple à mettre en œuvre et la précision obtenue avec, par exemple, $10^9$ expériences peut suffire dans la plupart des cas. Il existe des formules analytiques pour estimer la valeur de $\pi$, mais elles peuvent être plus complexes à implémenter.

\section{Moyenne de plusieurs expériences indépendantes}
\label{exp_independantes}

Pour l'instant, nous avons simplement tenté de trouver des valeurs approchées de $\pi$ en générant des points de manière aléatoire. Par conséquent, à condition que les expériences soient indépendantes, le résultat obtenu peut considérablement varier d'une expérience à l'autre. Ainsi, afin d'obtenir de meilleures estimations de $\pi$, il peut être intéressant de lancer la fonction de simulation \verb|sim_pi| définie précédemment plusieurs fois et de prendre la moyenne des différentes valeurs obtenues. 

Pour mieux comprendre la démarche, reprenons l'exercice consistant à calculer la valeur moyenne d'un lancer de dés (cf. TP2). Lors de chaque expérience, nous avons utilisé \emph{Mersenne Twister} pour générer un nombre aléatoire, et nous avons effectué un nombre $n$ d'expériences de manière indépendante. Nous avons enfin calculé la moyenne de tous les lancers.

Ici, la démarche est similaire. Nous utiliserons simplement la fonction \verb|sim_pi| pour obtenir des valeurs approchées de $\pi$. Les valeurs approchées de $\pi$ seront stockées dans un tableau qui contiendra les résultats. Il sera ainsi plus facile de les traiter et de les analyser par la suite.

Dans un premier temps, nous nous contenterons de calculer la moyenne empirique des expériences et les erreurs absolue et relative avec la valeur théorique de $\pi$. Nous calculerons les intervalles de confiance dans la section \ref{intervalles_confiance}.

Pour obtenir des expériences aléatoires indépendantes, il suffit de ne pas réinitialiser le générateur de nombres pseudo-aléatoires entre chaque expérience. Nous garderons donc l'initialisation par défaut de \emph{Mersenne Twister} et nous ne l'initialiserons qu'une seule fois au début du programme.

La méthode décrite ci-dessus est implémentée dans la fonction \verb|sim_pi_n| du fichier \verb|monte_carlo.c|. Elle prend en paramètre le nombre de points voulu pour chaque expérience et le nombre total d'expériences. Elle renvoie le tableau des résultats. La fonction \verb|mean_tab| permettra ensuite de calculer la moyenne des valeurs d'un tel tableau.

Voici les résultats obtenus avec 10 et 40 expériences:

\begin{figure}[ht]
    \centering
    \begin{tabular}{r|c|c|c|c}
        Nombres de points & Estimation de $\pi$ & Erreur absolue & Erreur relative & Temps d'exécution (en s) \\
         $10^3$ &  3.124400 & -0.017193 & 0.994527 & 0.000258 \\
         $10^6$ & 3.142208 & 0.000616 & 1.000196 & 0.054679 \\
         $10^9$ & 3.141569 & -0.000024 & 0.999992 & 60.658452 \\
    \end{tabular}
    \caption{Estimations de $\pi$ obtenues avec 10 expériences indépendantes}
    \label{tab:pi_10_exp}
\end{figure}

\begin{figure}[ht]
    \centering
    \begin{tabular}{r|c|c|c|c}
        Nombres de points & Estimation de $\pi$ & Erreur absolue & Erreur relative & Temps d'exécution (en s) \\
         $10^3$ & 3.132900 & -0.008693 & 0.997233 & 0.001054 \\
         $10^6$ & 3.141873 & 0.000280 & 1.000089 & 0.256745 \\
         $10^9$ & 3.141587 & -0.000006 & 0.999998 & 243.696953 \\
    \end{tabular}
    \caption{Estimations de $\pi$ obtenues avec 40 expériences indépendantes}
    \label{tab:pi_40_exp}
\end{figure}

On peut remarquer que plus le nombre d'expériences aléatoires indépendantes est grand, plus la marge d'erreur se rapproche de 0 pour l'erreur absolue et de 1 pour l'erreur relative. Augmenter le nombre d'expériences indépendantes nous permet donc d'avoir une amélioration de la précision de notre approximation de $\pi$. Cependant, cela se fait au détriment de la rapidité de l'exécution du programme.

\section{Calcul des intervalles de confiance autour de la moyenne empirique}
\label{intervalles_confiance}

Après avoir calculé les moyennes d'expériences aléatoires indépendantes, il peut être intéressant de calculer des intervalles de confiance autour de la moyenne obtenue afin d'avoir une idée plus fine de sa précision.

La méthode utilisée pour calculer l'intervalle de confiance est détaillée dans le sujet. Mais afin de faciliter la compréhension du rapport, elles seront brièvement rappelées ici.

Soit $X$ une variable aléatoire représentant le résultat d'une simulation stochastique, par exemple celle de la valeur de $\pi$ à l'aide de la méthode de Monte Carlo. Notons $(X_1, \ldots, X_n)$ l'ensemble des résultats de $n$ expériences aléatoires de $X$. Supposons que nos variables aléatoires $X_i$, avec $i$ un entier compris entre $1$ et $n$, sont répartis selon la même distribution gaussienne indépendante.

On calcule la moyenne empirique $\Bar{X}(n)$ de la manière suivante :

\begin{equation*}
    \Bar{X}(n) = \frac{1}{n} \sum_{i=1}^n X_i
\end{equation*}

D'après la loi des grands nombres, plus $n$ est grand, plus la valeur de $\Bar{X}(n)$ tend vers la moyenne théorique de $X$, notée $\mu$.

A partir de cette moyenne empirique, il est possible d'obtenir un estimateur non biaisé $S^2(n)$ de la variance de $X$, notée $\sigma^2$, de la variable aléatoire $X$ :

\begin{equation*}
    S^2(n) = \frac{1}{n-1} \sum_{i=1}^n \left[ X_i - \Bar{X}(n) \right]^2
\end{equation*}

Nous ne connaissons pas la valeur de l'écart type théorique $\sigma$ de $X$. Nous allons donc utiliser l'estimateur non biaisé $S^2(n)$ dans nos calculs.

On pose également la variable aléatoire $T(n)$ de la manière suivante :

\begin{equation*}
    T(n) = \frac{\Bar{X}(n) - \mu}{\sqrt{\frac{S^2(n)}{n}}}
\end{equation*}

$T(n)$ suit une loi de Student de degré $n-1$. Cette loi permet de corriger le fait que nous n'utiliserons qu'un estimateur de l'écart-type $\sigma$ et non sa valeur exacte. Nous utiliserons les quantiles $t_{n-1, 1-\frac{\alpha}{2}}$ de la loi de Student de degré $n-1$ pour obtenir un intervalle de confiance à $1-\alpha$.

Pour le reste du TP, nous poserons $\alpha = 0.05$. Cela nous permettra d'obtenir un intervalle de confiance à 0.95, c'est-à-dire, à 95 \%. Les valeurs de $t_{n-1, 1-\frac{\alpha}{2}}$ en fonction de $n$ sont données dans le sujet. À partir de ces valeurs, on calcule :

\begin{equation*}
    R = t_{n-1, 1-\frac{\alpha}{2}} \times \sqrt{\frac{S^2(n)}{n}}
\end{equation*}

Et on obtient enfin un intervalle de confiance à $1-\alpha$ pour notre variable aléatoire $X$ :

\begin{equation*}
    [\Bar{X} - R, \Bar{X} + R]
\end{equation*}

Deux fonctions ont été codées dans le fichier \verb|monte_carlo.c| pour calculer un tel intervalle de confiance :

\begin{itemize}
    \item \verb|quantile_student| prend en paramètre un entier n strictement positif et renvoie la valeur $t_{n-1, 1-\frac{\alpha}{2}}$ avec $\alpha = 0.05$.
    \item \verb|confidence_interval| prend en paramètre le tableau des résultats générés précédemment ainsi que le nombre d'expériences. Elle calcule la moyenne empirique et l'estimateur non biaisé de la variance avant de renvoyer les deux bornes de l'intervalle de confiance à 95 \%.
\end{itemize}

Voici les intervalles de confiance obtenus en prenant les résultats calculés dans la section \ref{exp_independantes} :

\begin{figure}[ht]
    \centering
    \begin{tabular}{r|c|c}
        Nombres de points & Estimation de $\pi$ & Intervalle de confiance \\
         $10^3$ & 3.132900 & [3.113721 , 3.152079] \\
         $10^6$ & 3.141873 & [3.141306 , 3.142439] \\
         $10^9$ & 3.141587 & [3.141570 , 3.141603] \\
    \end{tabular}
    \caption{Intervalles de confiance à 95 \% des estimations de $\pi$ obtenues avec 40 expériences indépendantes}
    \label{tab:pi_int_conf}
\end{figure}

On peut constater que les intervalles de confiance deviennent plus "petits" lorsque le nombre de points augmente. De plus, l'intervalle se resserre autour de la valeur de $\pi$.

\section{Conclusion}

Dans ce TP, nous avons mis en pratique la méthode de Monte Carlo afin de calculer une valeur approchée de $\pi$. Cette méthode \emph{stochastique} utilise une source d'aléatoire et de l'échantillonnage par opposition aux modèles \emph{déterministes} qui utilisent de simples fonctions mathématiques. Elle est très utilisée pour les simulations où les modèles déterministes ne sont pas suffisamment précis pour pouvoir obtenir des résultats convaincants.

Cette méthode, très simple à mettre en œuvre, présente toutefois quelques limites. Ainsi, sa vitesse de convergence peut être très lente et il faut alors un nombre très important de points générés aléatoirement dans l'espace choisi pour pouvoir obtenir de bons résultats. 

Cependant, la méthode de Monte Carlo est incontournable pour résoudre certains problèmes, par exemple pour calculer des hyper-volumes dans les hyper-espaces ou pour remplir un espace de points uniformément répartis. En effet, il n'existe pas toujours de modèles mathématiques précis pour résoudre ces problèmes, et quand ils existent, ils sont souvent insolubles.

Enfin, l'échantillonnage de Monte Carlo est beaucoup utilisé pour l'entraînement de modèles d'intelligence artificielle, qui doivent traiter un nombre très important de données.

\end{document}