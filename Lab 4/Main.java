import complex_rabbit_sim.Simulation;

public class Main {
    public static void main(String[] args) {
        Simulation sim = new Simulation(30);

        System.out.println(sim);

        while (sim.getMonth() < 240) {
            sim.nextYear();
            System.out.println(sim);
        }
    }
}
