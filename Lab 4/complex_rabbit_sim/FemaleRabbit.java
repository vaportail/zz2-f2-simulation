package complex_rabbit_sim;

import java.util.ArrayList;
import java.util.List;

public class FemaleRabbit extends Rabbit {
    @Override
    public String toString() {
        return "F";
    }

    public List<Rabbit> giveBirth() {
        List<Rabbit> rabbits = new ArrayList<>();

        // On donne naissance à de nouveaux lapins seulement si la femelle en est capable

        if (this.getState() != RabbitState.CHILD) {
            // Déterminer le nombre de portées qu'aura la femelle dans l'année

            // Elle aura au minimum 3 portées et au maximum 9
            int nb_litters = Simulation.getRand().nextInt(1, 4) + Simulation.getRand().nextInt(1, 4) + Simulation.getRand().nextInt(1, 4);

            for (int i = 0; i < nb_litters; i++) {
                // Calculer le nombre de nouveaux lapins de manière aléatoire

                int nb_new_rabbits = Simulation.getRand().nextInt(3, 6);

                // Ajouter les nouveaux lapins à la simlation

                for (int j = 0; j < nb_new_rabbits; j++) {
                    // Créer de nouveaux lapins

                    rabbits.add(getNewRabbit());
                }
            }
        }

        return rabbits;
    }
}
