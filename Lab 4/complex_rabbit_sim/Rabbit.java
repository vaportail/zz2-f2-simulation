package complex_rabbit_sim;

public abstract class Rabbit {
    private int age;
    private final int maturity;
    private RabbitState state;

    public Rabbit() {
        this.age = 0;
        this.state = RabbitState.CHILD;
        this.maturity = Simulation.getRand().nextInt(5, 8);
    }

    public int getAge() {
        return age;
    }

    public RabbitState getState() {
        return state;
    }

    public int getMaturity() {
        return maturity;
    }

    public static Rabbit getNewRabbit() {
        Rabbit r;

        if (Simulation.getRand().nextDouble() <= 0.5) {
            r = new MaleRabbit();
        } else {
            r = new FemaleRabbit();
        }

        return r;
    }

    public boolean isSurviving() {
        boolean survive = false;
        double probability = Simulation.getRand().nextDouble();

        if (this.getState() == RabbitState.CHILD) {
            if (probability < 0.35) {
                survive = true;
            }
        } else if (this.getState() == RabbitState.ADULT) {
            if (probability < 0.6) {
                survive = true;
            }
        } else if (this.getState() == RabbitState.OLD) {
            // L'espérance de vie du lapin baisse de 10 % tous les ans
            if (probability < (0.6 - ((this.getAge() - 120) / 120))) {
                survive = true;
            }
        }

        return survive;
    }

    public void nextMonth() {
        this.age++;

        if (this.getAge() == this.getMaturity()) {
            this.state = RabbitState.ADULT;
        } else if (this.getAge() > 120) {
            this.state = RabbitState.OLD;
        }
    }
}
