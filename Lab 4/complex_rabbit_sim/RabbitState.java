package complex_rabbit_sim;

public enum RabbitState {
    CHILD,
    ADULT,
    OLD
}
