package complex_rabbit_sim;

import java.util.ArrayList;
import java.util.List;

public class Simulation {
    private int month;
    private final List<Rabbit> rabbits;
    private final List<Rabbit> newborn_rabbits;

    private static int[] array = {0x123, 0x234, 0x345, 0x456};
    private static MersenneTwister rand = new MersenneTwister(array);

    public Simulation() {
        this.month = 0;
        rabbits = new ArrayList<>();
        newborn_rabbits = new ArrayList<>();

        rabbits.add(new MaleRabbit());
        rabbits.add(new FemaleRabbit());
    }

    public Simulation(int nb_rabbits) {
        this.month = 0;
        rabbits = new ArrayList<>();
        newborn_rabbits = new ArrayList<>();

        for (int i = 0; i < nb_rabbits; i++) {
            rabbits.add(Rabbit.getNewRabbit());
        }
    }

    public int getMonth() {
        return month;
    }

    public static MersenneTwister getRand() {
        return rand;
    }

    @Override
    public String toString() {
        return this.month + " mois (" + this.month / 12 + " ans) - " + this.rabbits.size() + " lapins";
    }

    public void nextMonth() {
        month++;

        for (Rabbit r : rabbits) {
            r.nextMonth();
        }

        // Création aléatoire de nouveaux couples ?
        // Séparation aléatoire de couples existants ?

        // Nouvelle année

        if (month % 12 == 0) {
            // Mort des lapins
            rabbits.removeIf(r -> !r.isSurviving());

            // Naissance de nouveaux lapins
            for (Rabbit r : rabbits) {
                if (r instanceof FemaleRabbit)  {
                    newborn_rabbits.addAll(((FemaleRabbit) r).giveBirth());
                }
            }

            // Ajout des lapins nés
            rabbits.addAll(newborn_rabbits);
            newborn_rabbits.clear();
        }
    }

    public void nextYear() {
        for (int i = 0; i < 12; i++) {
            nextMonth();
        }
    }
}
