#include <stdio.h>

void next_month(int * young_rabbits, int * adult_rabbits) {
    // Les couples de lapins adultes donnent naissance à de nouveaux couples de lapins
    int new_young_rabbits = *adult_rabbits;

    // Les lapins enfants deviennent adultes
    *adult_rabbits += *young_rabbits;

    // On prend enfin en compte les nouveaux lapins nés
    *young_rabbits = new_young_rabbits;
}

int main(int argc, char * argv[]) {
    int young_rabbits = 1;
    int adult_rabbits = 0;

    for (int month = 0; month < 40; month++) {
        printf("Mois %d : %d couples de lapins au total (%d enfants + %d adultes)\n", month, young_rabbits + adult_rabbits, young_rabbits, adult_rabbits);

        next_month(&young_rabbits, &adult_rabbits);
    }
    
    return 0;
}