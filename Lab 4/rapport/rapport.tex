\documentclass[french]{article}

\usepackage[utf8]{inputenc}
\usepackage{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{graphicx}

\usepackage[margin=3cm]{geometry}

\usepackage{mathtools}
\usepackage{amssymb}

\usepackage{hyperref}

\usepackage{listings}
\usepackage{algorithm2e}

\setlength{\parskip}{1ex plus 0.5ex minus 0.2ex}

\title{TP 4 - Croissance d'une population de lapins}
\author{Valentin PORTAIL, \emph{Enseignant :} David HILL}
\date{20 décembre 2024}

\begin{document}

\thispagestyle{empty} 

\maketitle

\begin{figure*}[ht]
    \centering
    \includegraphics[width=0.8\linewidth]{rabbits-4883006_1280.jpg}
\end{figure*}

\pagebreak

\tableofcontents
\listoffigures

\pagebreak

\section{Introduction}

Jusqu'ici, nous avons essentiellement utilisé des simulations pour étudier et analyser des modèles purement mathématiques. Ainsi, nous avons, par exemple, utilisé des générateurs de nombres pseudo-aléatoires pour reproduire différentes lois de distribution ou pour estimer des valeurs mathématiques comme $\pi$.

Dans un cadre plus général, la simulation désigne l'ensemble des méthodes et des applications mises en œuvre pour reproduire des systèmes réels, le plus souvent par ordinateur, pour pouvoir étudier plus facilement leur comportement et leur évolution au cours du temps. Généralement, elle est utilisée lorsqu'il n'est pas possible d'étudier directement le système réel, que ce soit par sa taille ou par sa complexité. Dans ce cas, on construit des modèles, basés sur un ensemble de suppositions et d'approximations sur le système réel, afin de rendre son analyse plus facile et plus rapide.

Dans ce TP, nous allons mettre en œuvre plusieurs modèles, déterministes et stochastiques, afin de simuler l'évolution d'une population de lapins au cours du temps. Ces modèles rentrent dans le cadre des simulations dites "bio-inspirées" qui tentent de reproduire des phénomènes naturels. La modélisation nous permet ici de reproduire et de comprendre ce que nous pouvons observer dans notre environnement.

Nous commencerons d'abord par un modèle simple et déterministe basé sur la suite de Fibonacci. Puis nous définirons plusieurs règles, basées ou non sur de l'aléatoire, afin de construire un modèle plus complexe et plus réaliste qui nous permettra d'évaluer de manière plus précise l'évolution de la population de lapins.

\section{Modélisation simple : La suite de Fibonacci}

En 1202, dans son ouvrage \emph{Liber abaci}, le mathématicien italien Léonard de Pise, également connu sous le nom de Leonardo Fibonacci, propose un modèle déterministe simple pour décrire la croissance d'une population de lapins.

Dans ce modèle, nous étudions le nombre de couples de lapins mois après mois. Plusieurs règles ont été définies :

\begin{itemize}
    \item La simulation commence au mois 0 avec un seul couple de lapereaux.
    \item Les lapins deviennent adultes à l'âge de deux mois.
    \item Chaque couple de lapins adultes donne naissance tous les mois à un nouveau couple de lapereaux.
    \item On suppose que les lapins ne meurent jamais.
\end{itemize}

Le schéma ci-dessous présente l'évolution du nombre de couples de lapins au cours du temps. On peut observer que le nombre de couples de lapins au mois $n$, noté $F_n$, lorsque $n \geq 2$, est égal à la somme des nombres de couples de lapins au mois $n-2$ et au mois $n-1$. L'évolution du nombre de couples de lapins au cours du temps peut donc être représentée par la suite mathématique suivante, aussi appelée suite de Fibonacci :

\begin{equation*}
    \left\{
    \begin{array}{l}
        F_0 = F_1 = 1 \\
        \forall n \geq 2, F_n = F_{n-2} + F_{n-1}
    \end{array}
    \right.
\end{equation*}

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\linewidth]{Fibonacci_Rabbits.png}
    \caption{Description de l'évolution du nombre de lapins selon la suite de Fibonacci \\ Romain, CC BY-SA 4.0, via Wikimedia Commons}
    \label{fig:fibo-rabbits}
\end{figure}

Le programme \verb|fast_rabbit_sim.c|, écrit en langage C et présent dans le dossier \verb|fast_rabbit_sim|, propose une implémentation de cette modélisation simplifiée et déterministe de l'évolution d'une population de lapins. Ici, on ne s'intéresse qu'au nombre de lapins présents dans la simulation. Il n'est donc pas utile de reproduire dans les détails leur comportement. Cela sera fait dans la simulation complexe.

La procédure \verb|next_month| prend en paramètres des pointeurs sur le nombre de lapins enfants et le nombre de lapins adultes et modifie la population de lapins selon les règles données par Fibonacci :

\begin{itemize}
    \item Les couples de lapins adultes donnent naissance à de nouveaux couples de lapereaux.
    \item Les lapereaux déjà présents le mois précédent deviennent adultes.
    \item Les lapereaux nouvellement nés sont pris en compte dans la population.
\end{itemize}

Le programme principal commence au mois 0 avec 1 couple de lapereaux et aucun couple de lapins adultes. En exécutant le programme, par exemple ici sur 20 mois, on retrouve bien les différents termes de la suite de Fibonacci :

\begin{verbatim}
Mois 0 : 1 couples de lapins au total (1 enfants + 0 adultes)
Mois 1 : 1 couples de lapins au total (0 enfants + 1 adultes)
Mois 2 : 2 couples de lapins au total (1 enfants + 1 adultes)
Mois 3 : 3 couples de lapins au total (1 enfants + 2 adultes)
Mois 4 : 5 couples de lapins au total (2 enfants + 3 adultes)
Mois 5 : 8 couples de lapins au total (3 enfants + 5 adultes)
Mois 6 : 13 couples de lapins au total (5 enfants + 8 adultes)
Mois 7 : 21 couples de lapins au total (8 enfants + 13 adultes)
Mois 8 : 34 couples de lapins au total (13 enfants + 21 adultes)
Mois 9 : 55 couples de lapins au total (21 enfants + 34 adultes)
Mois 10 : 89 couples de lapins au total (34 enfants + 55 adultes)
Mois 11 : 144 couples de lapins au total (55 enfants + 89 adultes)
Mois 12 : 233 couples de lapins au total (89 enfants + 144 adultes)
Mois 13 : 377 couples de lapins au total (144 enfants + 233 adultes)
Mois 14 : 610 couples de lapins au total (233 enfants + 377 adultes)
Mois 15 : 987 couples de lapins au total (377 enfants + 610 adultes)
Mois 16 : 1597 couples de lapins au total (610 enfants + 987 adultes)
Mois 17 : 2584 couples de lapins au total (987 enfants + 1597 adultes)
Mois 18 : 4181 couples de lapins au total (1597 enfants + 2584 adultes)
Mois 19 : 6765 couples de lapins au total (2584 enfants + 4181 adultes)
\end{verbatim}

\section{Modélisation complexe}

Comme évoqué précédemment, la modélisation déterministe basée sur la suite de Fibonacci est très simple à mettre en œuvre. Cependant, les résultats obtenus ne sont pas réalistes. Ils ne tiennent notamment pas compte de la mort des lapins, de leurs conditions de naissance ou encore du nombre de portées par gestation. Le modèle de Fibonacci est donc difficilement transposable dans la réalité.

Dans cette section, nous allons donc réfléchir à plusieurs critères permettant d'obtenir un modèle d'évolution de populations de lapins plus précis et plus réaliste que le modèle précédent. Pour cela, nous allons rajouter plusieurs paramètres stochastiques afin notamment de faire varier l'espérance de vie des lapins, la probabilité d'avoir des nouveaux-nés sur une année, le nombre de portées par gestation \ldots

Certains de ces choix sont basés sur une interprétation personnelle du problème. Les résultats obtenus peuvent donc varier considérablement en fonction du modèle et des paramètres choisis. 

\subsection{Analyse générale de l'architecture du programme}

Afin de mieux appréhender ce modèle et de pouvoir l'implémenter plus facilement, nous utiliserons le paradigme de programmation \emph{orienté objets}. Les différentes variables et fonctions seront ainsi regroupées au sein de petits objets indépendants les uns des autres et qui interagissement entre eux à l'aide de méthodes. 

L'idée est de regrouper ensemble les méthodes agissant sur un même type d'objet. L'idée est d'avoir un code plus simple à comprendre et mieux structuré. De plus, cette approche nous permet de gagner en modularité. Il sera ainsi plus facile à l'avenir de rajouter de nouvelles méthodes et de nouvelles classes sans avoir à réécrire l'ensemble du code.

Le langage de programmation orienté objets choisi pour implémenter ce modèle est \emph{Java}, mais il aurait été possible de l'implémenter dans un autre langage de programmation orienté objets comme le C++ ou le C\#.

Le langage Java a été choisi pour sa facilité de prise en main et sa grande portabilité. Cependant, l'exécution des différentes instructions du programme se fait dans la machine virtuelle Java, contrairement au C++ où les instructions sont directement compilées en langage machine. Cela peut avoir un impact sur les performances du programme, que ce soit en termes de rapidité d'exécution ou en termes d'utilisation mémoire.

L'ensemble des fichiers Java correspondant à la simulation se trouvent dans le répertoire et paquet \verb|complex_rabbit_sim|. Un fichier \verb|Main.java| est toutefois présent dans le même dossier que ce rapport afin de pouvoir rapidement lancer la simulation sur un exemple simple.

Le diagramme de classes UML ci-dessous présente les différentes classes et méthodes utilisées dans ce programme. Les constructeurs et les accesseurs ne sont toutefois pas représentés afin de gagner en lisibilité.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\linewidth]{651e7380.png}
    \caption{Diagramme UML du modèle complexe choisi pour représenter l'évolution d'un population de lapins au cours du temps}
    \label{fig:uml-rabbits}
\end{figure}

Plusieurs grands changements de modélisation ont été faits, en plus de passer à un modèle objet, par rapport à la simulation basée sur la suite de Fibonacci. Tout d'abord, les lapins sont désormais étudiés individuellement et non par couples. Chaque lapin possédera ses propres paramètres et ses propres caractéristiques qui auront une influence sur l'ensemble de la simulation.

Ensuite, la simulation est, dans l'ensemble, basée sur un pas annuel et les différentes évolutions du modèle au cours de la simulation (naissances, décès\ldots) ne seront prises en compte qu'au bout d'un an. Pour faciliter les modifications futures dans la simulation, un pas mensuel sera également conservé.

\subsection{Modélisation des lapins}

Le comportement général des lapins est géré dans la classe \verb|Rabbit|. Chaque lapin va garder en mémoire son \emph{âge}, exprimé en mois, son \emph{âge de maturité}, lui aussi exprimé en mois, ainsi que son \emph{état}. 

À sa création, le lapin est considéré comme un \emph{enfant}. Il devient \emph{adulte} lorsqu'il atteint son âge de maturité et il devient \emph{âgé} au bout de 120 mois, c'est-à-dire 10 ans, après sa naissance. L'âge de maturité du lapin est fixé aléatoirement entre 5 et 8 mois au moment de la naissance du lapin. Cependant, afin de simplifier la simulation, la gestion des naissances et des décès se fera selon une échelle annuelle et non mensuelle. On pourra donc supposer que l'âge de maturité d'un lapin sera fixé à 1 an. L'échelle mensuelle reste toutefois présente afin de pouvoir plus facilement faire évoluer la simulation à l'avenir.

La classe \verb|Rabbit| est une classe abstraite et il n'est donc pas possible d'instancier directement un lapin. Il n'est possible d'instancier que des lapins \emph{mâles} et \emph{femelles}  qui sont définis respectivement dans les sous-classes \verb|MaleRabbit| et \verb|FemaleRabbit|. Seules les lapines seront capables de donner naissance à de nouveaux lapins.

\subsection{Naissance de nouveaux lapins}

Pour chaque lapine, la gestion des nouvelles naissances se fait tous les ans grâce à la méthode \verb|giveBirth()| de la classe \verb|FemaleRabbit|. Cette méthode, qui ne prend pas de paramètres, renverra une liste contenant tous les nouveaux lapins qui naîtront de cette lapine cette année.

Pour commencer, on commence par déterminer le nombre de portées auxquelles la lapine donnera naissance pour l'année en cours. Selon le cahier des charges, une lapine peut avoir entre 3 et 9 portées par an, mais elle a plus de chances d'en avoir entre 5 et 7. La loi de probabilité à utiliser ne doit donc pas être uniforme et doit tenir compte de cette répartition.

La loi de probabilités choisie pour cette simulation est la suivante : on génère 3 entiers aléatoires compris entre 1 et 3. Le nombre de portées correspond ainsi à la somme de ces 3 valeurs aléatoires. Ainsi, on a bien un résultat minimal de 3, un résultat maximal de 9 et un probabilité plus importante d'obtenir un résultat autour de 6. Voici l'histogramme représentant la probabilité d'obtenir chaque valeur :

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\linewidth]{repartition_portees.png}
    \caption{Répartition des différents résultats théoriques possibles pour la loi de probabilités décrivant le nombre de portées}
    \label{fig:rep-portees}
\end{figure}

Au sein de chaque portée, entre 3 et 6 lapins pourront voir le jour. Ce nombre est choisi aléatoirement selon une loi uniforme. Ensuite, les différents lapins sont créés grâce à la méthode statique \verb|getNewRabbit()| de la classe \verb|Rabbit| qui renvoie un nouveau lapin lorsqu'elle est appelée. Cette méthode a 50 \% de chances de renvoyer un lapin mâle et 50 \% de chances de envoyer un lapin femelle. Les lapins nouvellement générés seront ensuite ajoutés à une liste qui sera renvoyée par la méthode \verb|giveBirth()|.

\subsection{Mortalité des lapins}

Pour savoir si un lapin va mourir ou survivre pour la prochaine année, on utilise la méthode \verb|isSurviving()| de la classe \verb|Rabbit|. Cette méthode renvoie de manière aléatoire un booléen valant \emph{true} si le lapin survit et \emph{false} s'il meurt. La probabilité d'obtenir une valeur ou l'autre varie en fonction de l'âge du lapin :

\begin{itemize}
    \item Si le lapin est \emph{enfant}, il a 35 \% de chances de survivre.
    \item Si le lapin est \emph{adulte}, il a 60 \% de chances de survivre.
    \item Si le lapin est \emph{âgé}, c'est-à-dire s'il a plus de 10 ans, sa probabilité de survivre décroît de 10 \% chaque année. Par exemple, si le lapin a 12 ans, il aura $60 - 2 \times 10 = 40 \%$ de chances de survivre.
\end{itemize}

Avec cette modélisation, la durée de vie maximale d'un lapin est donc de 16 ans. En effet, à 16 ans, sa probabilité de survie sera de $60 - 6 * 10 = 0 \%$. Il sera donc, dans tous les cas, considéré comme mort pour la prochaine année.

\subsection{Environnement}

La gestion de l'ensemble des lapins générés se fait dans la classe \verb|Simulation|. Elle garde en mémoire le \emph{mois} courant, une liste contenant l'ensemble des \emph{lapins} et une liste où seront ajoutés les \emph{nouveaux-nés}.

La classe \verb|Simulation| possède également un attribut de classe \verb|rand| qui contient une instance du générateur de nombres pseudo-aléatoires \emph{Mersenne Twister}. Ce générateur sera appelé par toutes les autres classes qui ont besoin de générer des nombres de manière pseudo-aléatoire. On a ainsi un seul générateur pour toute la simulation.

La méthode \verb|toString()| permet de renvoyer une chaîne de caractères contenant la plupart des informations sur la simulation. On retrouve ainsi le nombre de mois qui se sont écoulés depuis le début de la simulation (et le nombre d'années d'écoulées) ainsi que le nombre de lapins.

La méthode \verb|nextMonth()| permet d'avancer la simulation d'un mois. L'âge de chaque lapin présent dans la simulation augmente ainsi d'un mois. Si le numéro du mois est un multiple de 12, alors un an supplémentaire s'est écoulé. Il faut donc gérer les naissances et les décès.

On commence par gérer les décès en appelant la méthode \verb|isSurviving()| sur chaque lapin présent dans la simulation. Si elle envoie \emph{false}, le lapin est retiré de la liste. Toutes ces opérations peuvent être condensées en un seul appel de la méthode \verb|removeIf()| sur la liste de lapins.

En revanche, la gestion des naissances nécessite l'utilisation d'une autre liste, qui sera appelée \verb|newborn_rabbits|. Pour chaque lapine présente dans la simulation, on appelle la méthode \verb|giveBirth()| sur elle et on ajoute tous les nouveaux lapins à la liste \verb|newborn_rabbits|. Pour finir, tous les nouveaux lapins sont ajoutés à la liste initiale, appelée \verb|rabbits|.

La méthode \verb|nextYear()| permet d'avancer la simulation d'un an en appelant 12 fois la méthode \verb|nextMonth()|. Pour le moment, aucun traitement supplémentaire n'est prévu dans cette méthode, mais de nouveaux traitements pourront être ajoutés par la suite au gré des extensions du modèle choisi.
 
\subsection{Résultats obtenus}

L'initialisation de Mersenne Twister se fait dans la simulation en utilisant un même tableau d'entiers, qui contient les mêmes valeurs que dans la version en C proposée par Makoto Matsumoto. Cela nous permettra d'avoir des résultats reproductibles à chaque fois que la simulation est lancée.

Nous lançons la simulation avec un nombre de lapins différent au mois 0. Les résultats sont présentés dans le tableau ci-dessous.

\begin{figure}[ht]
    \centering
    \begin{tabular}{r|c|c|c|c}
        Nombre d'années & Expérience 1 & Expérience 2 & Expérience 3 & Expérience 4 \\
        0 an & 2 & 3 & 4 & 30 \\
        1 an & 27 & 19 & 23 & 269 \\
        2 ans & 279 & 128 & 301 & 2 234 \\
        3 ans & 2 401 & 940 & 2 310 & 16 552 \\
        4 ans & 18 227 & 6 797 & 19 098 & 131 320 \\
        5 ans & 142 968 & 53 470 & 151 022 & 1 017 795 \\
        6 ans & 1 111 378 & 416 866 & 1 178 943 & 7 935 806 \\
        7 ans & 8 651 339 & 3 254 487 & 9 199 861 & 61 871 341 \\
        8 ans & 67 455 824 & 25 375 255 & 71 800 495 & ERREUR \\
        9 ans & ERREUR & ERREUR & ERREUR \\
    \end{tabular}
    \caption{Résultats de la simulation pour plusieurs expériences}
    \label{tab:resultats-lapins}
\end{figure}

Nous pouvons d'abord voir que cette simulation est très sensible à l'aléatoire. En effet, avec 3 lapins au mois 0, la population globale augmente moins rapidement avec 2 ou 4 lapins au mois 0. Dans l'ensemble, on observe aussi que cette population augmente de manière exponentielle. En effet, en termes d'ordre de grandeur, la population est multipliée par 10 chaque année.

\section{Conclusion}

Dans ce TP, nous avons vu différentes stratégies pour modéliser et suivre l'évolution d'une population de lapins. Cet exercice est très commun en simulation, où nous devons régulièrement estimer l'évolution de systèmes réels.

Pour estimer l'évolution de la population de lapins au cours du temps, nous avons commencé par implémenter un modèle très simple basé sur la suite mathématique de Fibonacci. Ce modèle purement déterministe et centré sur les couples de lapins est très simple à mettre en œuvre. Cependant, il ne tient pas compte de la mort des lapins au cours du temps et donne donc des résultats peu réalistes et peu précis.

Pour pallier à ces problèmes, nous avons proposé un deuxième modèle stochastique que nous avons implémenté en Java, un langage de programmation orienté objets. Ce modèle est désormais basé sur les individus plutôt que sur les couples et prend en compte de manière plus précise les naissances et les décès des différents lapins. Le choix du modèle objet nous permet également de gagner en modularité, ce qui permettra , à l'avenir, d'étendre plus facilement la simulation avec de nouvelles fonctionnalités. Cependant, le modèle proposé est très sensible à l'aléatoire et la moindre modification de la configuration de départ peut grandement faire varier les résultats.

Plusieurs améliorations sont encore possibles pour le modèle stochastique proposé dans le TP. Afin d'avoir une reproduction des lapins plus "réaliste", il pourrait être intéressant d'ajouter une mécanique de couples au modèle. Un lapin pourrait ainsi se mettre en couple avec un autre lapin. Seuls les lapins en couple seraient ainsi capables de donner naissance à de nouveaux lapins.

À l'avenir, il serait également possible d'ajouter au modèle des ressources limitées ou bien de tenir compte de prédateurs ou de maladies afin de déterminer le taux de mortalité des lapins.

\end{document}